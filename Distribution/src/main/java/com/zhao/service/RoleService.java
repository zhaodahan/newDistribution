package com.zhao.service;

import java.util.List;

import com.zhao.po.Role;

/** 
* @author 作者 E-mail: 
* @version 创建时间：2018年1月31日 下午2:30:07 
* 类说明 
*/
public interface RoleService {

	/**
	 * 获取所有角色信息
	 */
	List<Role> getAll();

}
 