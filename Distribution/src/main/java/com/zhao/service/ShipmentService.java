package com.zhao.service;

import java.util.List;

import com.zhao.po.Shipment;
import com.zhao.po.vo.OrderDetail;
import com.zhao.po.vo.OrderItemDetail;

/** 
* @author 作者 E-mail: 
* @version 创建时间：2018年1月29日 下午5:16:46 
* 类说明 
*/
public interface ShipmentService {


	/*
	 * 装运部分订单明细
	 */
	void saveShip(List<OrderItemDetail> pickList, Shipment shipment);

	
	
	/**
	 * 根据carid，来查询装运对象
	 */
	Shipment getShipmentByCar(Integer carid);

}
 