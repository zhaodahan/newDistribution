package com.zhao.service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.zhao.po.User;
import com.zhao.po.vo.UserDetail;

/**
 * @author 作者 E-zhao:
 * @version 创建时间：2017年11月18日 下午4:57:04 类说明
 */
public interface UserService {

	/**
	 * 查询所有的用户的列表
	 * @param user 
	 * 
	 * @return
	 */
	List<UserDetail> getAll(UserDetail user);

	/**
	 * 添加驾驶员
	 * 
	 * @param User
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	void add(UserDetail user) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException;


	/**
	 * 根据传入的对象更新
	 * 
	 * @param UserDetail
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	void updateUser(UserDetail User) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException;

	/**
	 * 更具id来删除用户
	 * @param id
	 */
	void deleteUser(Integer id);

	/**
	 * 根据id来查询
	 * @param userId
	 * @return
	 */
	User getById(int userId);



}
