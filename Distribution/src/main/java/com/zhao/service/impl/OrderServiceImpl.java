package com.zhao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.OrderMapper;
import com.zhao.po.vo.OrderDetail;
import com.zhao.po.vo.OrderUserDetail;
import com.zhao.service.CarService;
import com.zhao.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderMapper orderMapper;

	@Autowired
	private CarService carService;

	/**
	 * 查询列表
	 * 
	 * @return
	 */
	public List<OrderUserDetail> getAll(OrderUserDetail orderDetail) {
		// TODO Auto-generated method stub
		return orderMapper.getAll(orderDetail);
	}

	/**
	 * 添加订单
	 * 
	 * @param OrderDetail
	 */
	public void add(OrderDetail OrderDetail) {
		// TODO Auto-generated method stub
		orderMapper.insertSelective(OrderDetail);
	}

	/**
	 * 更新订单
	 * 
	 * @param order
	 */
	public void updateorder(OrderDetail order) {
		// TODO Auto-generated method stub
		orderMapper.updateByPrimaryKey(order);
	}

	/**
	 * 根据id来进行查询
	 * 
	 * @param orderid
	 * @return
	 */
	@Override
	public OrderDetail getOrderDetail(Integer orderid) {
		// TODO Auto-generated method stub
		return orderMapper.getOrderDetail(orderid);
	}

	
	
	@Override
	public void add(OrderUserDetail OrderDetail) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateorder(OrderUserDetail order) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Boolean dealOrder(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void pickOrder(int orderid) {
		// TODO Auto-generated method stub
		
	}

	

}
