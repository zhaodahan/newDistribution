package com.zhao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.OrderShipMapper;
import com.zhao.dao.ShipmentMapper;
import com.zhao.po.OrderShip;
import com.zhao.po.Shipment;
import com.zhao.po.vo.OrderDetail;
import com.zhao.po.vo.OrderItemDetail;
import com.zhao.service.ShipmentService;

/** 
* @author 作者 E-mail: 
* @version 创建时间：2018年1月29日 下午5:17:33 
* 类说明 
*/
@Service
public class ShipmentServiceImpl implements ShipmentService {
	@Autowired
	private ShipmentMapper shipmentMapper;

	@Autowired
	private OrderShipMapper orderShipMapper;
	
	/**
	 * 因为order订单表和shipment装运表时多对多
	 * 
	 * 1： 订单中的订单条目的装运信息全部存储在ordership表中
	 * 
	 * 2： ship表中存储的只是这次装运的一些基本信息
	 * 
	 */

	/**
	 * 装运部分订单明细
	 */
	@Override
	public void saveShip(List<OrderItemDetail> pickList, Shipment shipment) {
		// TODO Auto-generated method stub
		// 1： 获得订单id
		Integer orderid=pickList.get(0).getOrderid();
		//2: 向shipment表中插入ship对象，并获得其返回的自增主键id
		shipmentMapper.insertSelective(shipment);
		Integer shipid=shipment.getShipid();
		
		//3.遍历订单明细，依次插入ordership记录
		List<OrderItemDetail> Items = pickList;
		int sequence=1;
		for (OrderItemDetail item : Items) {
			OrderShip orderShip = new OrderShip();
			orderShip.setOrderid(orderid);
			orderShip.setShipid(shipid);
			orderShip.setOrdersequence(sequence++);
			//这种情况是车辆能够完全容纳订单的
			orderShip.setIsshipcount(item.getProductcount());
			orderShipMapper.insertSelective(orderShip);
		}
	}

	
	@Override
	public Shipment getShipmentByCar(Integer carid) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
 