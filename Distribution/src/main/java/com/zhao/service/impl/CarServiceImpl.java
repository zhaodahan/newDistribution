package com.zhao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.CarMapper;
import com.zhao.dao.ProductMapper;
import com.zhao.po.Car;
import com.zhao.po.Product;
import com.zhao.service.CarService;
import com.zhao.utils.BackPackageUtil;
import com.zhao.utils.CommonUtils;
import com.zhao.utils.ListConvertAdapter;
import com.zhao.utils.SerialNumber;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarMapper carMapper;

	// @Autowired
	// private ProductMapper productMapper;

	/**
	 * 查询所有的车辆
	 * 
	 * @return
	 */
	public List<Car> getAll() {
		// TODO Auto-generated method stub
		return carMapper.getList(null);
	}

	/**
	 * 添加车
	 * 
	 * @param Car
	 */
	public void add(Car Car) {
		if (Car != null) {
			carMapper.insertSelective(Car);
		}
	}

	/**
	 * 在进行更新请求的时候
	 * 
	 * @param id
	 * @return
	 */
	public Car getCar(Integer id) {
		// TODO Auto-generated method stub
		return carMapper.selectByPrimaryKey(id);
	}

	/**
	 * 更新车辆信息
	 * 
	 * @param car
	 */
	public void updateCar(Car car) {
		// 进行有选择的更新
		carMapper.updateByPrimaryKeySelective(car);
	}

	@Override
	public void batchDelete(String[] id) {
		// TODO Auto-generated method stub
		carMapper.batchDelete(id);
	}

	/**
	 * 根据id来单独删除 这里需要注意的是车辆的id是字符串
	 * 
	 * @param id
	 */
	public void deleteCar(Integer ids) {
		// TODO Auto-generated method stub
		carMapper.deleteByPrimaryKey(ids);
	}

	/**
	 * 从我们库存的车中找出一辆
	 * 
	 * @return
	 */
	// public Car findCar() {
	// TODO Auto-generated method stub
	// 从库存中找出可用的车的集合，然后从中随机选出一个；
	// CarInfoExample carInfoExample = new CarInfoExample();
	// Criteria carCriteria = carInfoExample.createCriteria();
	// // 选出车辆可用的车的集合
	// carCriteria.andCarstateEqualTo("Y");
	// List<Car> cars = carMapper.selectByExampleWithItem(carInfoExample);
	//
	// // 这里应该是随机的从集合中选择一个元素.取第一个？
	//
	// return cars != null ? cars.get(0) : null;
	// }

	/**
	 * 车辆装货：将商品货物装上车，并修改商品的状态为已经拣货 这里是批量拣货，一次拣一个集合的货
	 * 
	 * 将商品装车思路： 1. 使用背包算法获得，能够装车的item集合 2. 调用dao层方法更新item表中的carid。 3.
	 * 从总的Item集合中去掉已经装车的item 4. 如果还有没有装车的商品，重新分配车进行装车
	 * 
	 * @param car
	 * @param items
	 */
	//
	// public void pickItem(Car car, List<ItemInfo> items) {
	// // 这里需要判断一下所有商品是否能够一次性的装完，如果不能一次性的装完，就再获取一次车再装一次，直至装完
	// do {
	//
	// List<ItemInfo> fillList = new BackPackageUtil(car, items).backpack();
	//
	// // 通过工具类ListConvertAdapter 获取集合中的元素属性的集合
	// List<Integer> itemIds = new ListConvertAdapter<Integer,
	// ItemInfo>(fillList, "itemId").getElements();
	// // 这里通过背包算法来筛选出能放入车中的商品
	// if (itemIds != null) {
	// itemInfoMapper.updateCarIDByItemIDs(car.getCarid(), itemIds);
	// }
	//
	// items.removeAll(fillList);
	// if (items.size() > 0) {
	// // 商品没有装完，还需要车
	// car = ChangeCar();
	// }
	//
	// } while (items.size() > 0);
	//
	// }

	private Car ChangeCar() {
		// TODO Auto-generated method stub
		return findCar();
	}

	/**
	 * 这个方法用来修改
	 * 
	 * @param car
	 */
	public void ChangeCarState(Car car) {
		carMapper.updateByPrimaryKeySelective(car);
	}

	/**
	 * 从车库中选取一辆车:(从车库选车实现思路：)
	 * 
	 * 可以从数据库中查询出所有的“可用的” 车辆
	 * 
	 * 随机从中选出一辆
	 */
	@Override
	public Car findCar() {
		Car car = new Car();
		// 设置要查询的车辆的状态是可用的
		car.setCarstatus(CommonUtils.TYUESTATUS);
		List<Car> cars = carMapper.getList(car);
		if (cars != null && cars.size() > 0) {
			//选取List中的第一个
			return cars.get(0);
		}

		return null;
	}

	@Override
	public void pickItem(Car car, List<Product> products) {
		// TODO Auto-generated method stub

	}

}
