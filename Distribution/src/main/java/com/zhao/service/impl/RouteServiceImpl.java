package com.zhao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.OptimalPathMapper;
import com.zhao.dao.RouteMapper;
import com.zhao.po.OptimalPath;
import com.zhao.po.Route;
import com.zhao.service.RouteService;
import com.zhao.utils.MyWeightedGraph;

/**
 * @author 作者 E-mail:
 * @version 创建时间：2018年1月29日 下午3:11:24 类说明
 */
@Service
public class RouteServiceImpl implements RouteService {

	@Autowired
	private RouteMapper routeMapper;

	@Autowired
	private OptimalPathMapper optimalPathMapper;

	public Route getRouteByStartAndEnd(String start, String end) {
		// 在Route表中起点和终点是联合唯一约束，
		return routeMapper.findByStartAndEnd(start, end);
	}


	/**
	 * 这里修改了路线，难么就需要重新构建路径图，那么就需要清空已经计算好的最优路径
	 */
	@Override
	public void updateLine(Route line) {
		// TODO Auto-generated method stub
         routeMapper.updateByPrimaryKeySelective(line);
	}


	/**
	 * 通过Dijkstra(迪杰斯特拉)算法来计算得到最优的路径
	 */
	@Override
	public OptimalPath choseLine(Integer start, Integer end) {
		// TODO Auto-generated method stub

		// 1.要使用算法获取最短的路径信息就要查询出所有的路径存储信息
		// 获取素所有的
		List<Route> roadLines = routeMapper.getList(null);
		// 2.使用算法工具类来获取最短的路径信息(这个算法可能会有问题，没有测试)
		MyWeightedGraph graph = new MyWeightedGraph(roadLines, MyWeightedGraph.LENGTH);
		return graph.getMinStep(start, end);
	}
	

	/**
	 * 这里是查询我们存储的最优路径表，看是否有已经计算好的，我们拿来重用，不需要重新去计算
	 */
	@Override
	public OptimalPath OptimalPath(Integer start, Integer end) {
		// TODO Auto-generated method stub
		return optimalPathMapper.findByStartAndEnd(start, end);
	}


	/**
	 * 将计算出的最短路径存储到数据库中
	 */
	@Override
	public void addOptimalPath(OptimalPath line) {
		// TODO Auto-generated method stub
		optimalPathMapper.insertSelective(line);
	}


	/**
	 * 清空我们存储的最优路径
	 */
	@Override
	public void clearOptimalPath() {
		// TODO Auto-generated method stub
		optimalPathMapper.deleteByPrimaryKey(null);
	}

}
