package com.zhao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.RoleMapper;
import com.zhao.po.Role;
import com.zhao.service.RoleService;

/** 
* @author 作者 E-mail: 
* @version 创建时间：2018年1月31日 下午2:30:46 
* 类说明 
*/

@Service
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleMapper roleMapper;

	@Override
	public List<Role> getAll() {
		// TODO Auto-generated method stub
		return roleMapper.getAll();
	}

}
 