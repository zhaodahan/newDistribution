package com.zhao.service; 
import com.zhao.po.OptimalPath;
/** 
* @author 作者 E-mail: 
* @version 创建时间：2018年1月29日 下午2:58:37 
* 类说明 
*/
import com.zhao.po.Route;
public interface RouteService {

	/**
	 * 根据起点和终点来获取这条路线
	 * @param start
	 * @param end
	 * @return
	 */
    Route getRouteByStartAndEnd(String start,String end );

    /**
     * 更新路线信息
     * @param line
     */
	void updateLine(Route line);

	/**
	 * 更加起点和终点来选取最短路径
	 * @param start
	 * @param end
	 * @return
	 */
	OptimalPath choseLine(Integer start, Integer end);
	
	/**
	 * 查询我们存储的最优路径，获取最优的
	 * @param start
	 * @param end
	 * @return
	 */
	OptimalPath OptimalPath(Integer start, Integer end);

	/**
	 * 存储已经计算了的最优路径
	 * @param line
	 */
	void addOptimalPath(OptimalPath line);

	/**
	 * 清空我们的最优路径
	 */
	void clearOptimalPath();
}
 