package com.zhao.service;

import java.util.List;

import com.zhao.po.vo.OrderDetail;
import com.zhao.po.vo.OrderUserDetail;

/**
 * @author 作者 E-mail:
 * @version 创建时间：2017年11月18日 下午5:06:33 类说明
 */
public interface OrderService {
	/**
	 * 查询列表
	 * 
	 * @return
	 */
	List<OrderUserDetail> getAll(OrderUserDetail OrderDetail);

	/**
	 * 添加订单
	 * 
	 * @param OrderDetail
	 */
	void add(OrderUserDetail OrderDetail);

	/**
	 * 更新订单
	 * 
	 * @param order
	 */
	void updateorder(OrderUserDetail order);

	/**
	 * 根据id来进行查询
	 * 
	 * @param orderid
	 * @return
	 */
	OrderDetail getOrderDetail(Integer orderid);

	/**
	 * 这个方法是用来处理订单的
	 * 
	 * @param parseInt
	 */
	Boolean dealOrder(int id);

	/**
	 * 分配拣货
	 * 
	 * @param OrderUserDetail
	 */
	void pickOrder(int orderid);




	
}
