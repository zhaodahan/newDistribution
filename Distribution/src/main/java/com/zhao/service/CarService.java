package com.zhao.service;

import java.util.List;

import com.zhao.po.Car;
import com.zhao.po.Product;

/**
 * @author 作者 zhao:
 * @version 创建时间：2017年11月18日 下午4:51:56 类说明
 */
public interface CarService {

	/**
	 * 查询所有的车辆
	 * 
	 * @return
	 */
	List<Car> getAll();

	/**
	 * 添加车
	 * 
	 * @param Car
	 */
	void add(Car Car);

	/**
	 * 在进行更新请求的时候
	 * 
	 * @param id
	 * @return
	 */
	Car getCar(Integer id);

	/**
	 * 更新车辆信息
	 * 
	 * @param car
	 */
	void updateCar(Car car);

	/**
	 * 批量删除车辆信息
	 * 
	 * @param del_ids
	 */
	void batchDelete(String[] id);


	/**
	 * 从我们库存的车中找出一辆
	 * 
	 * @return
	 */
	Car findCar();


	/**
	 * 车辆装货：将商品货物装上车，并修改商品的状态为已经拣货 这里是批量拣货，一次拣一个集合的货
	 * 
	 * @param car
	 * @param items
	 */

	void pickItem(Car car, List<Product> products);

	/**
	 * 这个方法用来修改
	 * 
	 * @param car
	 */
	void ChangeCarState(Car car);

	
}
