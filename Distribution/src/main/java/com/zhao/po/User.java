package com.zhao.po;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class User {
    private Integer userid;

    private String username;

    private String userpassword;

    private Integer roleid;

    private Integer age;

    private String telphone;

    private Integer sendaddrid;

    private String usersex;

    private String useremaile;

    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date edittime;

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword == null ? null : userpassword.trim();
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone == null ? null : telphone.trim();
    }

    public Integer getSendaddrid() {
        return sendaddrid;
    }

    public void setSendaddrid(Integer sendaddrid) {
        this.sendaddrid = sendaddrid;
    }

    public String getUsersex() {
        return usersex;
    }

    public void setUsersex(String usersex) {
        this.usersex = usersex == null ? null : usersex.trim();
    }

    public String getUseremaile() {
        return useremaile;
    }

    public void setUseremaile(String useremaile) {
        this.useremaile = useremaile == null ? null : useremaile.trim();
    }

    public Date getEdittime() {
        return edittime;
    }

    public void setEdittime(Date edittime) {
        this.edittime = edittime;
    }
}