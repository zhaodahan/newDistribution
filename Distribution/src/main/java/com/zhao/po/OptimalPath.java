package com.zhao.po;

import java.math.BigDecimal;

public class OptimalPath {
    private Integer routedetailid;

    private Integer startid;

    private Integer endid;

    private String routedetail;

    private BigDecimal routecost;

    private Integer routetime;

    private BigDecimal routelength;

    private String routestate;

    public Integer getRoutedetailid() {
        return routedetailid;
    }

    public void setRoutedetailid(Integer routedetailid) {
        this.routedetailid = routedetailid;
    }

    public Integer getStartid() {
        return startid;
    }

    public void setStartid(Integer startid) {
        this.startid = startid;
    }

    public Integer getEndid() {
        return endid;
    }

    public void setEndid(Integer endid) {
        this.endid = endid;
    }

    public String getRoutedetail() {
        return routedetail;
    }

    public void setRoutedetail(String routedetail) {
        this.routedetail = routedetail == null ? null : routedetail.trim();
    }

    public BigDecimal getRoutecost() {
        return routecost;
    }

    public void setRoutecost(BigDecimal routecost) {
        this.routecost = routecost;
    }

    public Integer getRoutetime() {
        return routetime;
    }

    public void setRoutetime(Integer routetime) {
        this.routetime = routetime;
    }

    public BigDecimal getRoutelength() {
        return routelength;
    }

    public void setRoutelength(BigDecimal routelength) {
        this.routelength = routelength;
    }

    public String getRoutestate() {
        return routestate;
    }

    public void setRoutestate(String routestate) {
        this.routestate = routestate == null ? null : routestate.trim();
    }
}