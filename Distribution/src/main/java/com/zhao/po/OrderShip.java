package com.zhao.po;

public class OrderShip {
    private Integer ordershipid;

    private Integer orderid;

    private Integer shipid;

    private Integer ordersequence;

    private Integer isshipcount;

    public Integer getOrdershipid() {
        return ordershipid;
    }

    public void setOrdershipid(Integer ordershipid) {
        this.ordershipid = ordershipid;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public Integer getShipid() {
        return shipid;
    }

    public void setShipid(Integer shipid) {
        this.shipid = shipid;
    }

    public Integer getOrdersequence() {
        return ordersequence;
    }

    public void setOrdersequence(Integer ordersequence) {
        this.ordersequence = ordersequence;
    }

    public Integer getIsshipcount() {
        return isshipcount;
    }

    public void setIsshipcount(Integer isshipcount) {
        this.isshipcount = isshipcount;
    }
}