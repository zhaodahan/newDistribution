package com.zhao.po;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Shipment {
    private Integer shipid;

    private Integer shipperid;

    private Integer customerid;

    private Integer shiptype;

    private Integer shipstatus;

    private Integer statid;

    private Integer endid;

    private Integer carid;

    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date predicttime;

    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date actualtime;

    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date predictreachtime;

    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date actualreachtime;

    private BigDecimal predictshipcost;

    private BigDecimal actualshipcost;

    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date latestcaceltime;

    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date canceltime;

    private String operatdesc;

    public Integer getShipid() {
        return shipid;
    }

    public void setShipid(Integer shipid) {
        this.shipid = shipid;
    }

    public Integer getShipperid() {
        return shipperid;
    }

    public void setShipperid(Integer shipperid) {
        this.shipperid = shipperid;
    }

    public Integer getCustomerid() {
        return customerid;
    }

    public void setCustomerid(Integer customerid) {
        this.customerid = customerid;
    }

    public Integer getShiptype() {
        return shiptype;
    }

    public void setShiptype(Integer shiptype) {
        this.shiptype = shiptype;
    }

    public Integer getShipstatus() {
        return shipstatus;
    }

    public void setShipstatus(Integer shipstatus) {
        this.shipstatus = shipstatus;
    }

    public Integer getStatid() {
        return statid;
    }

    public void setStatid(Integer statid) {
        this.statid = statid;
    }

    public Integer getEndid() {
        return endid;
    }

    public void setEndid(Integer endid) {
        this.endid = endid;
    }

    public Integer getCarid() {
        return carid;
    }

    public void setCarid(Integer carid) {
        this.carid = carid;
    }

    public Date getPredicttime() {
        return predicttime;
    }

    public void setPredicttime(Date predicttime) {
        this.predicttime = predicttime;
    }

    public Date getActualtime() {
        return actualtime;
    }

    public void setActualtime(Date actualtime) {
        this.actualtime = actualtime;
    }

    public Date getPredictreachtime() {
        return predictreachtime;
    }

    public void setPredictreachtime(Date predictreachtime) {
        this.predictreachtime = predictreachtime;
    }

    public Date getActualreachtime() {
        return actualreachtime;
    }

    public void setActualreachtime(Date actualreachtime) {
        this.actualreachtime = actualreachtime;
    }

    public BigDecimal getPredictshipcost() {
        return predictshipcost;
    }

    public void setPredictshipcost(BigDecimal predictshipcost) {
        this.predictshipcost = predictshipcost;
    }

    public BigDecimal getActualshipcost() {
        return actualshipcost;
    }

    public void setActualshipcost(BigDecimal actualshipcost) {
        this.actualshipcost = actualshipcost;
    }

    public Date getLatestcaceltime() {
        return latestcaceltime;
    }

    public void setLatestcaceltime(Date latestcaceltime) {
        this.latestcaceltime = latestcaceltime;
    }

    public Date getCanceltime() {
        return canceltime;
    }

    public void setCanceltime(Date canceltime) {
        this.canceltime = canceltime;
    }

    public String getOperatdesc() {
        return operatdesc;
    }

    public void setOperatdesc(String operatdesc) {
        this.operatdesc = operatdesc == null ? null : operatdesc.trim();
    }
}