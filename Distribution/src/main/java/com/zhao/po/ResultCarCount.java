package com.zhao.po;

import java.math.BigDecimal;

/** 
* @author 作者 E-mail: 
* @version 创建时间：2018年3月10日 上午12:18:41 
* 类说明 : 这个是车辆统计的返回类； 车辆配送（体积 重量 件数 距离）
*/
public class ResultCarCount {

	private BigDecimal volume; //体积
	
	private BigDecimal weight; //重量
	
	private Integer   num;  //件数
	
	private BigDecimal distance; //距离	

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public BigDecimal getDistance() {
		return distance;
	}

	public void setDistance(BigDecimal distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return "ResultCarCount [volume=" + volume + ", weight=" + weight + ", num=" + num + ", distance=" + distance
				+ "]";
	}
	
	
}
 