package com.zhao.po;

public class Role {
    private Integer roleid;

    private String rolename;

    private String rolegenre;

    private String roledese;

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename == null ? null : rolename.trim();
    }

    public String getRolegenre() {
        return rolegenre;
    }

    public void setRolegenre(String rolegenre) {
        this.rolegenre = rolegenre == null ? null : rolegenre.trim();
    }

    public String getRoledese() {
        return roledese;
    }

    public void setRoledese(String roledese) {
        this.roledese = roledese == null ? null : roledese.trim();
    }
}