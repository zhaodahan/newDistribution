package com.zhao.po;

import java.math.BigDecimal;

public class Car {
    private Integer carid;

    private String platenum;

    private String carstatus;

    private BigDecimal maxweight;

    private BigDecimal carlength;

    private BigDecimal carwith;

    private BigDecimal carheight;

    private BigDecimal hascontain;

    private BigDecimal hasweight;

    private String notes;

    public Integer getCarid() {
        return carid;
    }

    public void setCarid(Integer carid) {
        this.carid = carid;
    }

    public String getPlatenum() {
        return platenum;
    }

    public void setPlatenum(String platenum) {
        this.platenum = platenum == null ? null : platenum.trim();
    }

    public String getCarstatus() {
        return carstatus;
    }

    public void setCarstatus(String carstatus) {
        this.carstatus = carstatus == null ? null : carstatus.trim();
    }

    public BigDecimal getMaxweight() {
        return maxweight;
    }

    public void setMaxweight(BigDecimal maxweight) {
        this.maxweight = maxweight;
    }

    public BigDecimal getCarlength() {
        return carlength;
    }

    public void setCarlength(BigDecimal carlength) {
        this.carlength = carlength;
    }

    public BigDecimal getCarwith() {
        return carwith;
    }

    public void setCarwith(BigDecimal carwith) {
        this.carwith = carwith;
    }

    public BigDecimal getCarheight() {
        return carheight;
    }

    public void setCarheight(BigDecimal carheight) {
        this.carheight = carheight;
    }

    public BigDecimal getHascontain() {
        return hascontain;
    }

    public void setHascontain(BigDecimal hascontain) {
        this.hascontain = hascontain;
    }

    public BigDecimal getHasweight() {
        return hasweight;
    }

    public void setHasweight(BigDecimal hasweight) {
        this.hasweight = hasweight;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes == null ? null : notes.trim();
    }
}