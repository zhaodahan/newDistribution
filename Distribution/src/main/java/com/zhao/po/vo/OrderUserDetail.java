package com.zhao.po.vo;

import com.zhao.po.Order;

/** 
* @author 作者 E-mail: 
* @version 创建时间：2018年1月31日 下午5:55:40 
* 类说明 : 这个类是订单关联用户的扩展类
*/
public class OrderUserDetail extends Order {

//	订单的拥有者
	private UserDetail owner;

	public UserDetail getOwner() {
		return owner;
	}

	public void setOwner(UserDetail owner) {
		this.owner = owner;
	}
	
}
 