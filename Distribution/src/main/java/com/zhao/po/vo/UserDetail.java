package com.zhao.po.vo;

import com.zhao.po.Address;
import com.zhao.po.Role;
import com.zhao.po.User;

/**
 * @author 作者 E-mail:
 * @version 创建时间：2018年1月30日 下午4:24:47 类说明 : 这个类是用户对象的扩展对象
 */
public class UserDetail extends User {

	private Role role;
	private Address address;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}


	@Override
	public String toString() {
		return "UserDetail [role=" + role + ", address=" + address + ", getRole()=" + getRole() + ", getAddress()="
				+ getAddress() + ", getUserid()=" + getUserid() + ", getUsername()=" + getUsername()
				+ ", getUserpassword()=" + getUserpassword() + ", getRoleid()=" + getRoleid() + ", getAge()=" + getAge()
				+ ", getTelphone()=" + getTelphone() + ", getSendaddrid()=" + getSendaddrid() + ", getUsersex()="
				+ getUsersex() + ", getUseremaile()=" + getUseremaile() + ", getEdittime()=" + getEdittime()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

}
