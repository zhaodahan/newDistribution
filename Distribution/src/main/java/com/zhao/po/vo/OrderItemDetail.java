package com.zhao.po.vo;

import com.zhao.po.OrderItem;
import com.zhao.po.Product;

/**
 * @author 作者 E-mail:
 * @version 创建时间：2018年2月1日 上午11:00:02 类说明 : 这里是订单条目明细扩展
 */
public class OrderItemDetail extends OrderItem {
	private Product product;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
