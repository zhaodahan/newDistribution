package com.zhao.po.vo;

import java.util.List;

import com.zhao.po.Order;

/**
 * @author 作者 E-mail:
 * @version 创建时间：2018年2月1日 上午10:37:16 类说明 : 这个类是用来显示订单详情的，他包含了订单的详细信息
 */
public class OrderDetail extends Order {
	private List<OrderItemDetail> itemList;

	public List<OrderItemDetail> getItems() {
		return itemList;
	}

	public void setItems(List<OrderItemDetail> itemList) {
		this.itemList = itemList;
	}

}
