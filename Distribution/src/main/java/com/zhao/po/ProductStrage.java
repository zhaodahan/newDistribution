package com.zhao.po;

public class ProductStrage {
    private Integer productstrageid;

    private Integer productid;

    private Integer productcount;

    private Integer respertoryid;

    public Integer getProductstrageid() {
        return productstrageid;
    }

    public void setProductstrageid(Integer productstrageid) {
        this.productstrageid = productstrageid;
    }

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    public Integer getProductcount() {
        return productcount;
    }

    public void setProductcount(Integer productcount) {
        this.productcount = productcount;
    }

    public Integer getRespertoryid() {
        return respertoryid;
    }

    public void setRespertoryid(Integer respertoryid) {
        this.respertoryid = respertoryid;
    }
}