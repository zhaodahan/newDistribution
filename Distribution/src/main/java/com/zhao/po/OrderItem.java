package com.zhao.po;

public class OrderItem {
    private Integer orderitemid;

    private Integer orderid;

    private Integer productid;

    private Integer productcount;

    private Integer ordersequence;

    public Integer getOrderitemid() {
        return orderitemid;
    }

    public void setOrderitemid(Integer orderitemid) {
        this.orderitemid = orderitemid;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    public Integer getProductcount() {
        return productcount;
    }

    public void setProductcount(Integer productcount) {
        this.productcount = productcount;
    }

    public Integer getOrdersequence() {
        return ordersequence;
    }

    public void setOrdersequence(Integer ordersequence) {
        this.ordersequence = ordersequence;
    }
}