package com.zhao.po;

import java.math.BigDecimal;

public class Route {
    private Integer routeid;

    private Integer startid;

    private Integer endid;

    private BigDecimal routelength;

    private BigDecimal routecost;

    private Integer routetime;

    private String routestate;

    private String ischange;

    private String routedesc;

    public Integer getRouteid() {
        return routeid;
    }

    public void setRouteid(Integer routeid) {
        this.routeid = routeid;
    }

    public Integer getStartid() {
        return startid;
    }

    public void setStartid(Integer startid) {
        this.startid = startid;
    }

    public Integer getEndid() {
        return endid;
    }

    public void setEndid(Integer endid) {
        this.endid = endid;
    }

    public BigDecimal getRoutelength() {
        return routelength;
    }

    public void setRoutelength(BigDecimal routelength) {
        this.routelength = routelength;
    }

    public BigDecimal getRoutecost() {
        return routecost;
    }

    public void setRoutecost(BigDecimal routecost) {
        this.routecost = routecost;
    }

    public Integer getRoutetime() {
        return routetime;
    }

    public void setRoutetime(Integer routetime) {
        this.routetime = routetime;
    }

    public String getRoutestate() {
        return routestate;
    }

    public void setRoutestate(String routestate) {
        this.routestate = routestate == null ? null : routestate.trim();
    }

    public String getIschange() {
        return ischange;
    }

    public void setIschange(String ischange) {
        this.ischange = ischange == null ? null : ischange.trim();
    }

    public String getRoutedesc() {
        return routedesc;
    }

    public void setRoutedesc(String routedesc) {
        this.routedesc = routedesc == null ? null : routedesc.trim();
    }
}