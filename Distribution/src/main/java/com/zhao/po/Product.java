package com.zhao.po;

import java.math.BigDecimal;

public class Product {
    private Integer productid;

    private String productname;

    private BigDecimal price;

    private String productkeyword;

    private String productcode;

    private String productcolor;

    private BigDecimal productlenth;

    private BigDecimal productwith;

    private BigDecimal productheight;

    private BigDecimal productweight;

    private String productdesc;

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer productid) {
        this.productid = productid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname == null ? null : productname.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductkeyword() {
        return productkeyword;
    }

    public void setProductkeyword(String productkeyword) {
        this.productkeyword = productkeyword == null ? null : productkeyword.trim();
    }

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode == null ? null : productcode.trim();
    }

    public String getProductcolor() {
        return productcolor;
    }

    public void setProductcolor(String productcolor) {
        this.productcolor = productcolor == null ? null : productcolor.trim();
    }

    public BigDecimal getProductlenth() {
        return productlenth;
    }

    public void setProductlenth(BigDecimal productlenth) {
        this.productlenth = productlenth;
    }

    public BigDecimal getProductwith() {
        return productwith;
    }

    public void setProductwith(BigDecimal productwith) {
        this.productwith = productwith;
    }

    public BigDecimal getProductheight() {
        return productheight;
    }

    public void setProductheight(BigDecimal productheight) {
        this.productheight = productheight;
    }

    public BigDecimal getProductweight() {
        return productweight;
    }

    public void setProductweight(BigDecimal productweight) {
        this.productweight = productweight;
    }

    public String getProductdesc() {
        return productdesc;
    }

    public void setProductdesc(String productdesc) {
        this.productdesc = productdesc == null ? null : productdesc.trim();
    }
}