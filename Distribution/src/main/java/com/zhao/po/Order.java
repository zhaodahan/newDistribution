package com.zhao.po;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Order {
    private Integer orderid;

    private Integer customerid;

    private Integer orderstatus;

    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date ordertime;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd") 
    private Date canceltime;

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public Integer getCustomerid() {
        return customerid;
    }

    public void setCustomerid(Integer customerid) {
        this.customerid = customerid;
    }

    public Integer getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(Integer orderstatus) {
        this.orderstatus = orderstatus;
    }

    public Date getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(Date ordertime) {
        this.ordertime = ordertime;
    }

    public Date getCanceltime() {
        return canceltime;
    }

    public void setCanceltime(Date canceltime) {
        this.canceltime = canceltime;
    }
}