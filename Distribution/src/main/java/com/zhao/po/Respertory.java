package com.zhao.po;

public class Respertory {
    private Integer repertoryid;

    private String repertoryname;

    private Integer adressid;

    private String repertorydesc;

    public Integer getRepertoryid() {
        return repertoryid;
    }

    public void setRepertoryid(Integer repertoryid) {
        this.repertoryid = repertoryid;
    }

    public String getRepertoryname() {
        return repertoryname;
    }

    public void setRepertoryname(String repertoryname) {
        this.repertoryname = repertoryname == null ? null : repertoryname.trim();
    }

    public Integer getAdressid() {
        return adressid;
    }

    public void setAdressid(Integer adressid) {
        this.adressid = adressid;
    }

    public String getRepertorydesc() {
        return repertorydesc;
    }

    public void setRepertorydesc(String repertorydesc) {
        this.repertorydesc = repertorydesc == null ? null : repertorydesc.trim();
    }
}