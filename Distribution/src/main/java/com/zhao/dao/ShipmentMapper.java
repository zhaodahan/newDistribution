package com.zhao.dao;

import com.zhao.po.Shipment;

public interface ShipmentMapper {
    int deleteByPrimaryKey(Integer shipid);

    int insert(Shipment record);

    int insertSelective(Shipment record);

    Shipment selectByPrimaryKey(Integer shipid);

    int updateByPrimaryKeySelective(Shipment record);

    int updateByPrimaryKeyWithBLOBs(Shipment record);

    int updateByPrimaryKey(Shipment record);
}