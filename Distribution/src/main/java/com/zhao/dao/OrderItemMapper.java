package com.zhao.dao;

import com.zhao.po.OrderItem;

public interface OrderItemMapper {
    int deleteByPrimaryKey(Integer orderitemid);

    int insert(OrderItem record);

    int insertSelective(OrderItem record);

    OrderItem selectByPrimaryKey(Integer orderitemid);

    int updateByPrimaryKeySelective(OrderItem record);

    int updateByPrimaryKey(OrderItem record);
}