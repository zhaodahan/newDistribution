package com.zhao.dao;

import com.zhao.po.ProductStrage;

public interface ProductStrageMapper {
    int deleteByPrimaryKey(Integer productstrageid);

    int insert(ProductStrage record);

    int insertSelective(ProductStrage record);

    ProductStrage selectByPrimaryKey(Integer productstrageid);

    int updateByPrimaryKeySelective(ProductStrage record);

    int updateByPrimaryKey(ProductStrage record);
}