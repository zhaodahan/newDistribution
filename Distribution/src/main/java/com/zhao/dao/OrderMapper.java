package com.zhao.dao;

import java.util.List;

import com.zhao.po.Order;
import com.zhao.po.vo.OrderDetail;
import com.zhao.po.vo.OrderUserDetail;

public interface OrderMapper {
    int deleteByPrimaryKey(Integer orderid);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer orderid);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    /**
     * 查询订单列表给用户
     * @param orderDetail
     * @return
     */
	List<OrderUserDetail> getAll(OrderUserDetail orderDetail);

	/**
	 * 根据订单id来查询订单明细
	 * @param orderid
	 * @return
	 */
	OrderDetail getOrderDetail(Integer orderid);

}