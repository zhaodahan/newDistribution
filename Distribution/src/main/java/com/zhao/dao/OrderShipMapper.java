package com.zhao.dao;

import com.zhao.po.OrderShip;

public interface OrderShipMapper {
    int deleteByPrimaryKey(Integer ordershipid);

    int insert(OrderShip record);

    int insertSelective(OrderShip record);

    OrderShip selectByPrimaryKey(Integer ordershipid);

    int updateByPrimaryKeySelective(OrderShip record);

    int updateByPrimaryKey(OrderShip record);
}