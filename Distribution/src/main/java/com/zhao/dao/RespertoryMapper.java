package com.zhao.dao;

import com.zhao.po.Respertory;

public interface RespertoryMapper {
    int deleteByPrimaryKey(Integer repertoryid);

    int insert(Respertory record);

    int insertSelective(Respertory record);

    Respertory selectByPrimaryKey(Integer repertoryid);

    int updateByPrimaryKeySelective(Respertory record);

    int updateByPrimaryKey(Respertory record);
}