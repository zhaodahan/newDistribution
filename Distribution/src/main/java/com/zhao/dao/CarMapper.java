package com.zhao.dao;

import java.util.List;

import com.zhao.po.Car;

public interface CarMapper {
    int deleteByPrimaryKey(Integer carid);

    int insert(Car record);

    int insertSelective(Car record);

    Car selectByPrimaryKey(Integer carid);

    int updateByPrimaryKeySelective(Car record);

    int updateByPrimaryKeyWithBLOBs(Car record);

    int updateByPrimaryKey(Car record);

    /**
     * 
     * @param car
     * @return
     */
	List<Car> getList(Car car);

	/**
	 * 批量删除汽车
	 * @param id
	 */
	void batchDelete(String[] ids);
}