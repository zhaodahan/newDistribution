package com.zhao.dao;

import org.apache.ibatis.annotations.Param;

import com.zhao.po.OptimalPath;

public interface OptimalPathMapper {
    int deleteByPrimaryKey(Integer routedetailid);

    int insert(OptimalPath record);

    int insertSelective(OptimalPath record);

    OptimalPath selectByPrimaryKey(Integer routedetailid);

    int updateByPrimaryKeySelective(OptimalPath record);

    int updateByPrimaryKey(OptimalPath record);

    /**
     * 根据起点和终点来获取最优的路径
     * @param start
     * @param end
     * @return
     */
	OptimalPath findByStartAndEnd(@Param("start") Integer start, @Param("end") Integer end);
}