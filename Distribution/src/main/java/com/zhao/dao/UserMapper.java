package com.zhao.dao;

import java.util.List;

import com.zhao.po.User;
import com.zhao.po.vo.UserDetail;

public interface UserMapper {
    int deleteByPrimaryKey(Integer userid);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userid);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    /**
     * 获取用户列表
     * @param object
     * @return
     */
	List<UserDetail> getAll(UserDetail userDetail);
}