package com.zhao.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zhao.po.Route;

public interface RouteMapper {
    int deleteByPrimaryKey(Integer routeid);

    int insert(Route record);

    int insertSelective(Route record);

    Route selectByPrimaryKey(Integer routeid);

    int updateByPrimaryKeySelective(Route record);

    int updateByPrimaryKeyWithBLOBs(Route record);

    int updateByPrimaryKey(Route record);

    /**
     * 根据起点和终点来查找路线
     * @param start
     * @param end
     * @return
     */
	Route findByStartAndEnd(@Param("start") String start,@Param("end") String end);

	/**
	 * 查询出数据库中的所有的路径信息
	 * route是传入的过滤信息
	 * @param object
	 * @return
	 */
	List<Route> getList(Route route);
}