package com.zhao.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zhao.exception.ErrorLineException;
import com.zhao.po.Msg;
import com.zhao.po.OptimalPath;
import com.zhao.po.Route;
import com.zhao.service.RouteService;
import com.zhao.utils.CommonUtils;
import com.zhao.utils.LogUtil;

@Controller
@RequestMapping("/line")
public class RouteController {

	@Autowired
	private RouteService routeService;

	/**
	 * 这个方法用来获取最短路径.传入起点和终点 这里页面需要传入什么样的参数？
	 * 通过各种请求获取到起点和终点的地址信息，通过前台传过来地址信息的id.这里的id是前台查询出来的
	 * 
	 * 
	 * @return
	 * @throws Exception
	 */

	/**
	 * 根据起点和终点来获取最短路径
	 */
	/**
	 * 要实现路径的选择需要注意下面的问题： 所有点之间的距离，费用都是存起来的。每次需要发货的时候，可以通过计算，得到最佳的路线。
	 * 这个路线可以放在表中记录起来，以便下次使用。 如果基础表中某两点间的权值发生变化时，所有与之相关的路线应被打上标记，在使用时重新计算。
	 * 路线的设计，以费用为优先，看需不需要考虑以时间为优先的因素。
	 * 
	 * (这个最短路径先存储在数据库中)
	 */
	@RequestMapping("/getLine")
	@ResponseBody
	public Msg getlines(String startId, String endId) {

		Integer start = Integer.valueOf(startId);
		Integer end = Integer.valueOf(endId);

		// 0.查询我们存储的最短路径中有没有我们要查的
		OptimalPath line = null;
		line = routeService.OptimalPath(start, end);

		if (line == null) {
			// 1：没有在存储的最优路径中，我们需要调用方法去计算出最优路劲放回，且将这个最优路径存储到最优路径表中
			line = routeService.choseLine(start, end);
			// 2：将计算出的最短路径存储起来
			if (line != null) {
				routeService.addOptimalPath(line);
				return Msg.success().add("line", line);
			}
		}

		return Msg.success().add("line", line);
	}

	/**
	 * 这里修改了路线，难么就需要重新构建路径图，那么就需要清空已经计算好的最优路径 下次再算的时候重新计算
	 * 
	 * @param line
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateLine/{lineid}", method = RequestMethod.PUT)
	@ResponseBody
	public Msg updateLine(@Valid Route line, BindingResult result) throws Exception {

		if (result.hasErrors()) {
			// 校验失败
			Map<String, Object> map = new HashMap<>();
			List<FieldError> errors = result.getFieldErrors();
			for (FieldError fieldError : errors) {
				System.out.println("错误的字段名：" + fieldError.getField());
				System.out.println("错误信息：" + fieldError.getDefaultMessage());
				map.put(fieldError.getField(), fieldError.getDefaultMessage());
			}
			return Msg.fail().add("errorFields", map);
		}

		line.setIschange(CommonUtils.HASCHANGE);
		routeService.updateLine(line);
		// 清空已经计算好了的最优路径
		routeService.clearOptimalPath();
		return Msg.success();

	}

	/**
	 * 添加路线： 添加路线肯定需要填写起点和终点
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addLine", method = RequestMethod.POST)
	@ResponseBody
	public Msg addLine(@Valid Route line, BindingResult result) {
		if (result.hasErrors()) {
			// 校验失败
			Map<String, Object> map = new HashMap<>();
			List<FieldError> errors = result.getFieldErrors();
			for (FieldError fieldError : errors) {
				System.out.println("错误的字段名：" + fieldError.getField());
				System.out.println("错误信息：" + fieldError.getDefaultMessage());
				map.put(fieldError.getField(), fieldError.getDefaultMessage());
			}
			return Msg.fail().add("errorFields", map);
		}

		// routeService.addLine(line);
		return Msg.success();

	}

}
