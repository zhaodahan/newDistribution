package com.zhao.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhao.po.Car;
import com.zhao.po.Msg;
import com.zhao.service.CarService;
import com.zhao.utils.CommonUtils;
import com.zhao.utils.LogUtil;

@Controller
@RequestMapping("/car")
public class CarController {

	@Autowired
	private CarService carService;
	// ==================先写简单的CRUD=================

	/**
	 * 这个方法是用来返回字符串，只针对浏览器的
	 * 
	 * @param pn
	 * @return
	 */
	// @RequestMapping("/getcars")
	public String getCars(@RequestParam(value = "pn", defaultValue = "1") Integer pn, Model model) throws Exception {
		// 这本不是一个分页查询，但是为了实现分页查询，在这里我们 引入了PageHelper插件
		PageHelper.startPage(pn, CommonUtils.PAGESIZE);
		List<Car> cars = carService.getAll();
		// 将我们查询的数据分装成pageInfo对象
		PageInfo<Car> page = new PageInfo<Car>(cars, CommonUtils.PAGESIZE);
		model.addAttribute("page", page);
		return "carlist.jsp";

	}

	/**
	 * 返回json数据可以提高扩展性。(不是浏览器也可以解析，其他平台类似Android) 这里要设想到： 这里的查询是涉及到分页的
	 * 
	 * @return
	 */
	@RequestMapping("/getcars")
	@ResponseBody
	public Msg getCars(@RequestParam(value = "pn", defaultValue = "1") Integer pn) throws Exception {
		// 这本不是一个分页查询，但是为了实现分页查询，在这里我们 引入了PageHelper插件
		PageHelper.startPage(pn, CommonUtils.PAGESIZE);
		List<Car> cars = carService.getAll();
		// 将我们查询的数据分装成pageInfo对象
		PageInfo<Car> page = new PageInfo<Car>(cars, CommonUtils.PAGESIZE);
		return Msg.success().add("page", page);

	}

	/**
	 * 车辆添加
	 * 
	 * @return
	 */
	@RequestMapping(value = "/addcar", method = RequestMethod.POST)
	@ResponseBody
	public Msg addCar(Car car) throws Exception {
		// 这里业务上需要保证前台提交的时候验证一下车牌号，车辆的各个属性不为空
		// 对要添加的汽车进行业务上的校验
		if (StringUtils.isBlank(car.getPlatenum()) || car.getCarlength() == null || car.getCarheight() == null
				|| car.getCarwith() == null || car.getMaxweight() == null) {
			// 添加的汽车不具备基本必须的属性
			return Msg.fail().add("erroMsg", "车辆基本信息未填写完全，无法添加");
		}
		carService.add(car);
		return Msg.success();
	}

	/**
	 * 下面完成车辆信息的修改功能 修改的逻辑是： 先查询一个，并将其回显在我们的表格之中，然后将将修改的进行更新
	 */

	/**
	 * 根据id查询车辆，用来查看车辆详情
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getCar/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Msg getCar(@PathVariable("id") String id)  {
		try {
			Car car = carService.getCar(Integer.valueOf(id));
			return Msg.success().add("car", car);
		} catch (Exception e) {
			// 控制台输出异常
			LogUtil.addErrorLog("汽车详情查询", "CarController.getCar", id, null, e);
			return Msg.fail().add("erroMsg", e.getMessage());
			
		}
       
	}

	/**
	 * 更行车辆信息 在这里进行需要请求方式是PUT，他在页面的表达方式是post请求+传送过来的数据加上“_method=put”
	 * 不过，Spring提供了HttpPutFormContentFilter，让我们可以在Ajax中直接发送PUT请求
	 * 
	 * @param car
	 * @return
	 */
	@RequestMapping(value = "/updateCar/{carId}", method = RequestMethod.PUT)
	@ResponseBody
	public Msg updateCar(Car car) throws Exception {
		if (StringUtils.isBlank(car.getPlatenum()) || car.getCarlength() == null || car.getCarheight() == null
				|| car.getCarwith() == null || car.getMaxweight() == null) {
			// 添加的汽车不具备基本必须的属性
			return Msg.fail().add("erroMsg", "车辆基本信息未填写完全，无法操作");
		}
		carService.updateCar(car);
		return Msg.success();
	}

	/**
	 * 批量删除，一个或者多个都调用同一个方法
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteCar/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public Msg deleteCar(@PathVariable("id") String[] id) throws Exception {
		// 批量删除:前台传入批量删除的车辆的id
	    if (id.length >0) {
			carService.batchDelete(id);
		}
		return Msg.fail().add("errorMsg", "无效的删除id");
	}

}
