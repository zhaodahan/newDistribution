package com.zhao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zhao.po.Car;
import com.zhao.po.Msg;
import com.zhao.po.Shipment;
import com.zhao.service.CarService;
import com.zhao.service.CountService;
import com.zhao.service.ShipmentService;

/**
 * 这个action的创建是用来满足统计的： 统计一些什么信息？ 暂时没有想出来 1：
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/count")
public class CountController {

	@Autowired
	private CountService countService;

	@Autowired
	private CarService carService;
	
	@Autowired
	private ShipmentService shipmentService;

	/**
	 * 这个方法用来统计e g: 车辆配送（体积 重量 件数 距离） 1： 传入的参数是车辆的id 2:
	 * 返回的是这辆车装运的货物的体积，重量，车辆装运的件数， 路线的距离 解决方法： 是否需要封装一个返回值对象
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/countCar/{carid}", method = RequestMethod.GET)
	@ResponseBody
	public Msg countCar(@PathVariable("carid") Integer carid) {
		/**
		 * 1.先获取到这辆车的对象 2.根据汽车对象找到对应转运对象
		 * 
		 */
		Car car = carService.getCar(carid);
		Shipment shipment=shipmentService.getShipmentByCar(carid);
		countService.countAllCar();
		return Msg.success();
	}

}
