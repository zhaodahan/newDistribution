package com.zhao.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhao.po.Msg;
import com.zhao.po.Role;
import com.zhao.po.User;
import com.zhao.po.vo.UserDetail;
import com.zhao.service.RoleService;
import com.zhao.service.UserService;
import com.zhao.utils.CommonUtils;
import com.zhao.utils.LogUtil;

@Controller
@RequestMapping("/User")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	// ==================先写简单的CRUD=================

	/**
	 * 返回json数据可以提高扩展性。(不是浏览器也可以解析，其他平台类似Android) 这里要设想到： 这里的查询是涉及到分页的
	 * 
	 * UserDetail: 这里的User是用来接收查询条件的
	 * 
	 * @return
	 */
	@RequestMapping("/getUsers")
	@ResponseBody
	public Msg getUsers(@RequestParam(value = "pn", defaultValue = "1") Integer pn, UserDetail User) throws Exception {
		// 这本不是一个分页查询，但是为了实现分页查询，在这里我们 引入了PageHelper插件
		PageHelper.startPage(pn, CommonUtils.PAGESIZE);
		List<UserDetail> Users = userService.getAll(User);
		// 将我们查询的数据分装成pageInfo对象
		PageInfo<UserDetail> page = new PageInfo<UserDetail>(Users, CommonUtils.PAGESIZE);
		return Msg.success().add("page", page);

	}

	/**
	 * 人员添加，这里添加人员的时候肯定会指定角色和地址。那么在这之前肯定还需要一个获取角色信息列表到前台的方法(返回list即可)
	 * 因为这里的角色表时可变的，所以角色和角色对应的id不能写死。 这里添加用户还会添加用户的地址信息 思路： 先用扩展po接收
	 * 
	 * @return
	 */
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	@ResponseBody
	public Msg addUser(@Valid UserDetail user, BindingResult result) {
		// 这里需要验证用户的详细信息，验证一下必填的信息。 * 这里使用了JSR303框架校验来简化校验
		// 校验规则写在po类上，这里暂时没有写，这里只是准备实现了这个功能
		if (result.hasErrors()) {
			// 校验失败
			Map<String, Object> map = new HashMap<>();
			List<FieldError> errors = result.getFieldErrors();
			for (FieldError fieldError : errors) {
				System.out.println("错误的字段名：" + fieldError.getField());
				System.out.println("错误信息：" + fieldError.getDefaultMessage());
				map.put(fieldError.getField(), fieldError.getDefaultMessage());
			}
			return Msg.fail().add("errorFields", map);
		}

		try {
			userService.add(user);
			return Msg.success();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// 控制台输出异常
			LogUtil.addErrorLog("用户添加", "UserController.addUser", user, null, e);
			return Msg.fail().add("erroMsg", e.getMessage());
		}
	}

	@RequestMapping("/getRoleList")
	@ResponseBody
	public Msg getRoleList() {
		List<Role> roles = roleService.getAll();
		return Msg.success().add("roles", roles);
	}

	/**
	 * 更新人员信息 在这里进行需要请求方式是PUT，他在页面的表达方式是post请求+传送过来的数据加上“_method=put”
	 * 不过，Spring提供了HttpPutFormContentFilter，让我们可以在Ajax中直接发送PUT请求
	 * 
	 * @param User
	 * @return
	 */
	@RequestMapping(value = "/updateUser/{UserId}", method = RequestMethod.PUT)
	@ResponseBody
	public Msg updateUser(@Valid UserDetail userDetail, BindingResult result) {
		if (result.hasErrors()) {
			// 校验失败
			Map<String, Object> map = new HashMap<>();
			List<FieldError> errors = result.getFieldErrors();
			for (FieldError fieldError : errors) {
				System.out.println("错误的字段名：" + fieldError.getField());
				System.out.println("错误信息：" + fieldError.getDefaultMessage());
				map.put(fieldError.getField(), fieldError.getDefaultMessage());
			}
			return Msg.fail().add("errorFields", map);
		}

		try {
			userService.updateUser(userDetail);
			return Msg.success();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// 控制台输出异常
			LogUtil.addErrorLog("用户修改", "UserController.updateUser", userDetail, null, e);
			return Msg.fail().add("erroMsg", e.getMessage());
		}

	}

	/*	*//**
			 * 根据id查询，用来信息表单回显 : 这里可能用不到，先注释了
			 * 
			 * @param id
			 * @return
			 *//*
			 * @RequestMapping(value = "/getUser/{id}", method =
			 * RequestMethod.GET)
			 * 
			 * @ResponseBody public Msg getUser(@PathVariable("id") String id)
			 * throws Exception { Integer Userid = Integer.parseInt(id); User
			 * user = userService.getUser(Userid); return
			 * Msg.success().add("User", user); }
			 */

	/**
	 * 这个方法是删除 单个批量二合一 批量删除：1-2-3(前端使用—来连接id) 单个删除：1
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteUser/{ids}", method = RequestMethod.DELETE)
	@ResponseBody
	public Msg deleteUser(@PathVariable("ids") String[] ids) throws Exception {
		// 批量删除,和单个删除
		if (ids.length > 0) {
			for (String id : ids) {
				userService.deleteUser(Integer.valueOf(id));
			}
			return Msg.success();
		}
		return Msg.fail();

	}

}
