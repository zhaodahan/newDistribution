package com.zhao.utils;

/**
 * 这个类主要是 用来定义一些常量的 分出来定义是为了在整个项目中使用的的时候更方便
 * 
 * @author Administrator
 */
public class CommonUtils {

	// 这里定义了分页的时候每页展示的记录数
	public static Integer PAGESIZE = 5;

	// 定义已经改变的标志符
	public static String HASCHANGE="Y";
	
//	定义一些状态的一些标志符
	public static String TYUESTATUS="Y";
	
	public static String FALSESTATUS="N";
}
