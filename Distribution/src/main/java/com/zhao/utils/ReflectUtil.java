package com.zhao.utils; 
/** 
* @author 作者 E-mail: 
* @version 创建时间：2017年11月21日 上午10:46:26 
* 类说明 : 这里是反射工具类
*/
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class ReflectUtil {
	
	/*
	 *  根据某个对象的名称和方法去执行该方法
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object execute(Object object,String methodName) {
		
	   
        Class clazz = object.getClass(); 
        Method method=null;
		try {
			method = clazz.getDeclaredMethod(methodName);
	        return method.invoke(object); 
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return method;  
	
	}
	
}
