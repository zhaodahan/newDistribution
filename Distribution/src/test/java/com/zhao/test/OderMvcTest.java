package com.zhao.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.alibaba.fastjson.JSONObject;
import com.zhao.po.Order;



/**
 * 使用Spring测试模块提供的测试请求功能，测试curd请求的正确性 Spring4测试的时候，需要servlet3. 0的支持
 * 
 * @author lfy
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:applicationContext.xml",
		"file:WebContent/WEB-INF/dispatcherServlet-servlet.xml" })
// 配置事务的回滚,对数据库的增删改都会回滚,便于测试用例的循环利用
@Rollback(value = true)
@Transactional(transactionManager = "transactionManager")
public class OderMvcTest {
	// 传入Springmvc的ioc
	@Autowired
	WebApplicationContext context;

	// 虚拟mvc请求，获取到处理结果。他被用来进行controller请求的单元测试
	MockMvc mockMvc;

	@Before
	public void initMokcMvc() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void testPage() throws Exception {
		// mockMvc.perform 模拟web请求拿到返回值
//		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/getorders").param("pn", "1")).andReturn();

		String responseString = mockMvc
				.perform(get("/getorders").contentType(MediaType.APPLICATION_JSON).param("pn", "1"))
				.andExpect(status().isOk()) // 返回的状态是200
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);

	}

	
//	订单无法删除： 如果他删除的话需要先将外键关联删除掉
	@Test
	public void testDelete() throws Exception {
		String responseString = mockMvc
				.perform(delete("/deleteorder/1")
						.contentType(MediaType.APPLICATION_JSON))
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);

	}

	@Test
	public void testGet() throws Exception {
		String responseString = mockMvc
				.perform(get("/getCar/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()) // 返回的状态是200
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);
	}

	/**
	 * 因为无法加载web.xml，所以更新无法测试
	 * @throws Exception
	 */
	@Test
	public void testUpdate() throws Exception {
		Order orderInfo=new Order();
//		 将对象转换成json字符串
		String requestJson = JSONObject.toJSONString(orderInfo); 

		String responseString = mockMvc
				.perform(put("/updateorder/1").contentType(MediaType.APPLICATION_JSON)
						.content(requestJson))
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);
	}
	
	
	/**
	 * 测试 添加
	 * @throws Exception
	 */
	@Test
	public void testAdd() throws Exception {
		
//		OrderInfo orderInfo=new OrderInfo();
//		orderInfo.setIsdealstate(true);
//        orderInfo.setCreatedate(new Date());
//		orderInfo.setReceiveraddress("重庆江津");
//		orderInfo.setReceivername("干尸");
//		orderInfo.setReceiverphone("15310384457");
//		 将对象转换成json字符串
//		String requestJson = JSONObject.toJSONString(orderInfo); 
		String requestJson = JSONObject.toJSONString(""); 

		System.out.println(requestJson);
		String responseString = mockMvc
				.perform(post("/addorder").contentType(MediaType.APPLICATION_JSON)
						.content(requestJson))
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);
	}
	
	
}
