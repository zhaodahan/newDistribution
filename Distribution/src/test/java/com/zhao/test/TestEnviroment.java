package com.zhao.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.zhao.dao.CarMapper;
import com.zhao.dao.OrderMapper;
import com.zhao.dao.RouteMapper;
import com.zhao.dao.UserMapper;
import com.zhao.po.Route;
import com.zhao.po.vo.OrderDetail;
import com.zhao.po.vo.UserDetail;

/**
 * 测试dao层的工作
 * 
 * @author lfy 推荐Spring的项目就可以使用Spring的单元测试，可以自动注入我们需要的组件 1、导入SpringTest模块
 *         2、@ContextConfiguration指定Spring配置文件的位置 3、直接autowired要使用的组件即可
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
// 配置事务的回滚,对数据库的增删改都会回滚,便于测试用例的循环利用
@Rollback(value = true)
@Transactional(transactionManager = "transactionManager")
public class TestEnviroment {

	// @Autowired
	// private OptimalPathMapper optimalPathMapper;

	@Autowired
	private CarMapper carMapper;

	@Autowired
	private OrderMapper orderMapper;

	@Test
	public void testCRUD() {

     OrderDetail orderDetail=orderMapper.getOrderDetail(1);

	}

}
