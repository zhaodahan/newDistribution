package com.zhao.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.zhao.po.Car;



/**
 * 使用Spring测试模块提供的测试请求功能，测试curd请求的正确性 Spring4测试的时候，需要servlet3. 0的支持
 * 
 * @author lfy
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:applicationContext.xml",
		"file:WebContent/WEB-INF/dispatcherServlet-servlet.xml" })
// 配置事务的回滚,对数据库的增删改都会回滚,便于测试用例的循环利用
@Rollback(value = true)
@Transactional(transactionManager = "transactionManager")
public class CarMvcTest {
	// 传入Springmvc的ioc
	@Autowired
	WebApplicationContext context;

	// 虚拟mvc请求，获取到处理结果。他被用来进行controller请求的单元测试
	MockMvc mockMvc;

	@Before
	public void initMokcMvc() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void testPage() throws Exception {
		// mockMvc.perform 模拟web请求拿到返回值
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/getcars").param("pn", "1")).andReturn();

		// 请求成功以后，请求域中会有pageInfo；我们可以取出pageInfo进行验证
		MockHttpServletRequest request = result.getRequest();
		PageInfo pi = (PageInfo) request.getAttribute("page");
		System.out.println(pi);
		System.out.println("当前页码：" + pi.getPageNum());
		System.out.println("总页码：" + pi.getPages());
		System.out.println("总记录数：" + pi.getTotal());
		System.out.println("在页面需要连续显示的页码");
		int[] nums = pi.getNavigatepageNums();
		for (int i : nums) {
			System.out.print(" " + i);
		}

	}

	@Test
	public void testDelete() throws Exception {
		String responseString = mockMvc
				.perform(delete("/deleteCar/zhao170918055513919818-zhao170918055513958063")
						.contentType(MediaType.APPLICATION_JSON))
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);

	}

	@Test
	public void testGet() throws Exception {
		String responseString = mockMvc
				.perform(get("/getCar/zhao170918055513919818").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()) // 返回的状态是200
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);
	}

	@Test
	public void testUpdate() throws Exception {
		 Car carInfo = new  Car();
//		 carInfo.setCarstate(false);
//		 将对象转换成json字符串
		String requestJson = JSONObject.toJSONString(carInfo); 
		System.out.println(carInfo);
		System.out.println(mockMvc
				.perform(put("/updateCar/zhao170918055513919818")));
		String responseString = mockMvc
				.perform(put("/updateCar/zhao170918055513919818").content(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
						.content(requestJson))
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);
	}
}
