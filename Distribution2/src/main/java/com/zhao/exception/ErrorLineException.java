package com.zhao.exception; 
/** 
* @author 作者 E-mail: 
* @version 创建时间：2017年11月22日 上午11:12:47 
* 类说明 
*/
public class ErrorLineException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4922780958941443804L;

	public ErrorLineException(String msg)  
    {  
        super(msg);  
    }  
}
 