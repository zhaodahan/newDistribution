package com.zhao.exception; 
/** 
* @author 作者 E-mail: 
* @version 创建时间：2017年11月20日 下午5:51:37 
* 类说明 
*/
public class ErrorAddressException extends Exception  
{  
    /**
	 * 
	 */
	private static final long serialVersionUID = 6075179540479956519L;

	public ErrorAddressException(String msg)  
    {  
        super(msg);  
    }  
}  