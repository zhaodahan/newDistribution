package com.zhao.po;

import javax.validation.constraints.Pattern;

/**
 * 这个类的数据添加是需要使用JSR303后台数据校验
 * @author Administrator
 * 验证前后台需要使用一致的校验规则
 */
public class DriverCarInfo {
    private Integer driverid;

    private String drivername;
    
    @Pattern(regexp="(/^[1][3,4,5,7,8][0-9]{9}$/)"
	,message="11位有效的电话号码")
    private String telphone;

    private Boolean driverstate;

    private Integer roleid;

    @Pattern(regexp="(^\\d{15}|\\d{18}$)"
    		,message="验证身份证号（15位或18位数字）")
    private String identityid;


//    这里是用来进行关联查询的，是让我们在查询驾驶员的时候查询出他对应的角色和权限
    private RoleInfo roleright;
    
    
    public RoleInfo getRoleright() {
		return roleright;
	}

	public void setRoleright(RoleInfo roleright) {
		this.roleright = roleright;
	}

	public Integer getDriverid() {
        return driverid;
    }

    public void setDriverid(Integer driverid) {
        this.driverid = driverid;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername == null ? null : drivername.trim();
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone == null ? null : telphone.trim();
    }

    public Boolean getDriverstate() {
        return driverstate;
    }

    public void setDriverstate(Boolean driverstate) {
        this.driverstate = driverstate;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getIdentityid() {
        return identityid;
    }

    public void setIdentityid(String identityid) {
        this.identityid = identityid == null ? null : identityid.trim();
    }

	@Override
	public String toString() {
		return "DriverCarInfo [driverid=" + driverid + ", drivername=" + drivername + ", telphone=" + telphone
				+ ", driverstate=" + driverstate + ", roleid=" + roleid + ", identityid=" + identityid + ", roleright="
				+ roleright + "]";
	}

    
}