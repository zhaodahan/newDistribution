package com.zhao.po;

/**
 * 这个po是我们用来存储计算出最短路径的
 * @author Administrator
 *
 */
public class LineDetail {
    private Integer lineid;

    private Integer startpoint;

    private Integer endpoint;

    private String linedetail;

    private Boolean linestate;

    public Integer getLineid() {
        return lineid;
    }

    public void setLineid(Integer lineid) {
        this.lineid = lineid;
    }

    public Integer getStartpoint() {
        return startpoint;
    }

    public void setStartpoint(Integer startpoint) {
        this.startpoint = startpoint;
    }

    public Integer getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Integer endpoint) {
        this.endpoint = endpoint;
    }

    public String getLinedetail() {
        return linedetail;
    }

    public void setLinedetail(String linedetail) {
        this.linedetail = linedetail == null ? null : linedetail.trim();
    }

    public Boolean getLinestate() {
        return linestate;
    }

    public void setLinestate(Boolean linestate) {
        this.linestate = linestate;
    }
}