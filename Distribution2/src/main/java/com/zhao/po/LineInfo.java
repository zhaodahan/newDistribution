package com.zhao.po;

public class LineInfo {
    private Integer lineid;

    private Integer linestart;

    private Integer lineend;

    private Float linelength;

    private Float linecost;

    private Integer linetime;

    private Boolean linestate;

    private Boolean ischange;

    private String linedesc;

    public Integer getLineid() {
        return lineid;
    }

    public void setLineid(Integer lineid) {
        this.lineid = lineid;
    }

    public Integer getLinestart() {
        return linestart;
    }

    public void setLinestart(Integer linestart) {
        this.linestart = linestart;
    }

    public Integer getLineend() {
        return lineend;
    }

    public void setLineend(Integer lineend) {
        this.lineend = lineend;
    }

    public Float getLinelength() {
        return linelength;
    }

    public void setLinelength(Float linelength) {
        this.linelength = linelength;
    }

    public Float getLinecost() {
        return linecost;
    }

    public void setLinecost(Float linecost) {
        this.linecost = linecost;
    }

    public Integer getLinetime() {
        return linetime;
    }

    public void setLinetime(Integer linetime) {
        this.linetime = linetime;
    }

    public Boolean getLinestate() {
        return linestate;
    }

    public void setLinestate(Boolean linestate) {
        this.linestate = linestate;
    }

    public Boolean getIschange() {
        return ischange;
    }

    public void setIschange(Boolean ischange) {
        this.ischange = ischange;
    }

    public String getLinedesc() {
        return linedesc;
    }

    public void setLinedesc(String linedesc) {
        this.linedesc = linedesc == null ? null : linedesc.trim();
    }
}