package com.zhao.po;

public class PointInfo {
    private Integer pointid;

    private String pointname;

    private String pointcity;

    private String pointdistrict;

    private String pointadress;

    private String pointdesc;

    public Integer getPointid() {
        return pointid;
    }

    public void setPointid(Integer pointid) {
        this.pointid = pointid;
    }

    public String getPointname() {
        return pointname;
    }

    public void setPointname(String pointname) {
        this.pointname = pointname == null ? null : pointname.trim();
    }

    public String getPointcity() {
        return pointcity;
    }

    public void setPointcity(String pointcity) {
        this.pointcity = pointcity == null ? null : pointcity.trim();
    }

    public String getPointdistrict() {
        return pointdistrict;
    }

    public void setPointdistrict(String pointdistrict) {
        this.pointdistrict = pointdistrict == null ? null : pointdistrict.trim();
    }

    public String getPointadress() {
        return pointadress;
    }

    public void setPointadress(String pointadress) {
        this.pointadress = pointadress == null ? null : pointadress.trim();
    }

    public String getPointdesc() {
        return pointdesc;
    }

    public void setPointdesc(String pointdesc) {
        this.pointdesc = pointdesc == null ? null : pointdesc.trim();
    }
}