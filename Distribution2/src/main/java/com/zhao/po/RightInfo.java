package com.zhao.po;

public class RightInfo {
    private Integer rightid;

    private String rightname;

    private String rightdesc;

    public Integer getRightid() {
        return rightid;
    }

    public void setRightid(Integer rightid) {
        this.rightid = rightid;
    }

    public String getRightname() {
        return rightname;
    }

    public void setRightname(String rightname) {
        this.rightname = rightname == null ? null : rightname.trim();
    }

    public String getRightdesc() {
        return rightdesc;
    }

    public void setRightdesc(String rightdesc) {
        this.rightdesc = rightdesc == null ? null : rightdesc.trim();
    }

	@Override
	public String toString() {
		return "RightInfo [rightid=" + rightid + ", rightname=" + rightname + ", rightdesc=" + rightdesc + "]";
	}
    
    
}