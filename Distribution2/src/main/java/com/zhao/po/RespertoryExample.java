package com.zhao.po;

import java.util.ArrayList;
import java.util.List;

public class RespertoryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RespertoryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRepertoryidIsNull() {
            addCriterion("repertoryID is null");
            return (Criteria) this;
        }

        public Criteria andRepertoryidIsNotNull() {
            addCriterion("repertoryID is not null");
            return (Criteria) this;
        }

        public Criteria andRepertoryidEqualTo(Integer value) {
            addCriterion("repertoryID =", value, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertoryidNotEqualTo(Integer value) {
            addCriterion("repertoryID <>", value, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertoryidGreaterThan(Integer value) {
            addCriterion("repertoryID >", value, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertoryidGreaterThanOrEqualTo(Integer value) {
            addCriterion("repertoryID >=", value, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertoryidLessThan(Integer value) {
            addCriterion("repertoryID <", value, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertoryidLessThanOrEqualTo(Integer value) {
            addCriterion("repertoryID <=", value, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertoryidIn(List<Integer> values) {
            addCriterion("repertoryID in", values, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertoryidNotIn(List<Integer> values) {
            addCriterion("repertoryID not in", values, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertoryidBetween(Integer value1, Integer value2) {
            addCriterion("repertoryID between", value1, value2, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertoryidNotBetween(Integer value1, Integer value2) {
            addCriterion("repertoryID not between", value1, value2, "repertoryid");
            return (Criteria) this;
        }

        public Criteria andRepertorynameIsNull() {
            addCriterion("repertoryName is null");
            return (Criteria) this;
        }

        public Criteria andRepertorynameIsNotNull() {
            addCriterion("repertoryName is not null");
            return (Criteria) this;
        }

        public Criteria andRepertorynameEqualTo(String value) {
            addCriterion("repertoryName =", value, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameNotEqualTo(String value) {
            addCriterion("repertoryName <>", value, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameGreaterThan(String value) {
            addCriterion("repertoryName >", value, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameGreaterThanOrEqualTo(String value) {
            addCriterion("repertoryName >=", value, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameLessThan(String value) {
            addCriterion("repertoryName <", value, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameLessThanOrEqualTo(String value) {
            addCriterion("repertoryName <=", value, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameLike(String value) {
            addCriterion("repertoryName like", value, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameNotLike(String value) {
            addCriterion("repertoryName not like", value, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameIn(List<String> values) {
            addCriterion("repertoryName in", values, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameNotIn(List<String> values) {
            addCriterion("repertoryName not in", values, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameBetween(String value1, String value2) {
            addCriterion("repertoryName between", value1, value2, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorynameNotBetween(String value1, String value2) {
            addCriterion("repertoryName not between", value1, value2, "repertoryname");
            return (Criteria) this;
        }

        public Criteria andRepertorypointIsNull() {
            addCriterion("repertoryPoint is null");
            return (Criteria) this;
        }

        public Criteria andRepertorypointIsNotNull() {
            addCriterion("repertoryPoint is not null");
            return (Criteria) this;
        }

        public Criteria andRepertorypointEqualTo(Integer value) {
            addCriterion("repertoryPoint =", value, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorypointNotEqualTo(Integer value) {
            addCriterion("repertoryPoint <>", value, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorypointGreaterThan(Integer value) {
            addCriterion("repertoryPoint >", value, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorypointGreaterThanOrEqualTo(Integer value) {
            addCriterion("repertoryPoint >=", value, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorypointLessThan(Integer value) {
            addCriterion("repertoryPoint <", value, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorypointLessThanOrEqualTo(Integer value) {
            addCriterion("repertoryPoint <=", value, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorypointIn(List<Integer> values) {
            addCriterion("repertoryPoint in", values, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorypointNotIn(List<Integer> values) {
            addCriterion("repertoryPoint not in", values, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorypointBetween(Integer value1, Integer value2) {
            addCriterion("repertoryPoint between", value1, value2, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorypointNotBetween(Integer value1, Integer value2) {
            addCriterion("repertoryPoint not between", value1, value2, "repertorypoint");
            return (Criteria) this;
        }

        public Criteria andRepertorydescIsNull() {
            addCriterion("repertoryDesc is null");
            return (Criteria) this;
        }

        public Criteria andRepertorydescIsNotNull() {
            addCriterion("repertoryDesc is not null");
            return (Criteria) this;
        }

        public Criteria andRepertorydescEqualTo(String value) {
            addCriterion("repertoryDesc =", value, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescNotEqualTo(String value) {
            addCriterion("repertoryDesc <>", value, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescGreaterThan(String value) {
            addCriterion("repertoryDesc >", value, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescGreaterThanOrEqualTo(String value) {
            addCriterion("repertoryDesc >=", value, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescLessThan(String value) {
            addCriterion("repertoryDesc <", value, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescLessThanOrEqualTo(String value) {
            addCriterion("repertoryDesc <=", value, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescLike(String value) {
            addCriterion("repertoryDesc like", value, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescNotLike(String value) {
            addCriterion("repertoryDesc not like", value, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescIn(List<String> values) {
            addCriterion("repertoryDesc in", values, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescNotIn(List<String> values) {
            addCriterion("repertoryDesc not in", values, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescBetween(String value1, String value2) {
            addCriterion("repertoryDesc between", value1, value2, "repertorydesc");
            return (Criteria) this;
        }

        public Criteria andRepertorydescNotBetween(String value1, String value2) {
            addCriterion("repertoryDesc not between", value1, value2, "repertorydesc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}