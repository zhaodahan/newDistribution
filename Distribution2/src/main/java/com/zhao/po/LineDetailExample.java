package com.zhao.po;

import java.util.ArrayList;
import java.util.List;

public class LineDetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public LineDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLineidIsNull() {
            addCriterion("LineID is null");
            return (Criteria) this;
        }

        public Criteria andLineidIsNotNull() {
            addCriterion("LineID is not null");
            return (Criteria) this;
        }

        public Criteria andLineidEqualTo(Integer value) {
            addCriterion("LineID =", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidNotEqualTo(Integer value) {
            addCriterion("LineID <>", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidGreaterThan(Integer value) {
            addCriterion("LineID >", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidGreaterThanOrEqualTo(Integer value) {
            addCriterion("LineID >=", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidLessThan(Integer value) {
            addCriterion("LineID <", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidLessThanOrEqualTo(Integer value) {
            addCriterion("LineID <=", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidIn(List<Integer> values) {
            addCriterion("LineID in", values, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidNotIn(List<Integer> values) {
            addCriterion("LineID not in", values, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidBetween(Integer value1, Integer value2) {
            addCriterion("LineID between", value1, value2, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidNotBetween(Integer value1, Integer value2) {
            addCriterion("LineID not between", value1, value2, "lineid");
            return (Criteria) this;
        }

        public Criteria andStartpointIsNull() {
            addCriterion("StartPoint is null");
            return (Criteria) this;
        }

        public Criteria andStartpointIsNotNull() {
            addCriterion("StartPoint is not null");
            return (Criteria) this;
        }

        public Criteria andStartpointEqualTo(Integer value) {
            addCriterion("StartPoint =", value, "startpoint");
            return (Criteria) this;
        }

        public Criteria andStartpointNotEqualTo(Integer value) {
            addCriterion("StartPoint <>", value, "startpoint");
            return (Criteria) this;
        }

        public Criteria andStartpointGreaterThan(Integer value) {
            addCriterion("StartPoint >", value, "startpoint");
            return (Criteria) this;
        }

        public Criteria andStartpointGreaterThanOrEqualTo(Integer value) {
            addCriterion("StartPoint >=", value, "startpoint");
            return (Criteria) this;
        }

        public Criteria andStartpointLessThan(Integer value) {
            addCriterion("StartPoint <", value, "startpoint");
            return (Criteria) this;
        }

        public Criteria andStartpointLessThanOrEqualTo(Integer value) {
            addCriterion("StartPoint <=", value, "startpoint");
            return (Criteria) this;
        }

        public Criteria andStartpointIn(List<Integer> values) {
            addCriterion("StartPoint in", values, "startpoint");
            return (Criteria) this;
        }

        public Criteria andStartpointNotIn(List<Integer> values) {
            addCriterion("StartPoint not in", values, "startpoint");
            return (Criteria) this;
        }

        public Criteria andStartpointBetween(Integer value1, Integer value2) {
            addCriterion("StartPoint between", value1, value2, "startpoint");
            return (Criteria) this;
        }

        public Criteria andStartpointNotBetween(Integer value1, Integer value2) {
            addCriterion("StartPoint not between", value1, value2, "startpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointIsNull() {
            addCriterion("EndPoint is null");
            return (Criteria) this;
        }

        public Criteria andEndpointIsNotNull() {
            addCriterion("EndPoint is not null");
            return (Criteria) this;
        }

        public Criteria andEndpointEqualTo(Integer value) {
            addCriterion("EndPoint =", value, "endpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointNotEqualTo(Integer value) {
            addCriterion("EndPoint <>", value, "endpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointGreaterThan(Integer value) {
            addCriterion("EndPoint >", value, "endpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointGreaterThanOrEqualTo(Integer value) {
            addCriterion("EndPoint >=", value, "endpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointLessThan(Integer value) {
            addCriterion("EndPoint <", value, "endpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointLessThanOrEqualTo(Integer value) {
            addCriterion("EndPoint <=", value, "endpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointIn(List<Integer> values) {
            addCriterion("EndPoint in", values, "endpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointNotIn(List<Integer> values) {
            addCriterion("EndPoint not in", values, "endpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointBetween(Integer value1, Integer value2) {
            addCriterion("EndPoint between", value1, value2, "endpoint");
            return (Criteria) this;
        }

        public Criteria andEndpointNotBetween(Integer value1, Integer value2) {
            addCriterion("EndPoint not between", value1, value2, "endpoint");
            return (Criteria) this;
        }

        public Criteria andLinedetailIsNull() {
            addCriterion("LineDetail is null");
            return (Criteria) this;
        }

        public Criteria andLinedetailIsNotNull() {
            addCriterion("LineDetail is not null");
            return (Criteria) this;
        }

        public Criteria andLinedetailEqualTo(String value) {
            addCriterion("LineDetail =", value, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailNotEqualTo(String value) {
            addCriterion("LineDetail <>", value, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailGreaterThan(String value) {
            addCriterion("LineDetail >", value, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailGreaterThanOrEqualTo(String value) {
            addCriterion("LineDetail >=", value, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailLessThan(String value) {
            addCriterion("LineDetail <", value, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailLessThanOrEqualTo(String value) {
            addCriterion("LineDetail <=", value, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailLike(String value) {
            addCriterion("LineDetail like", value, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailNotLike(String value) {
            addCriterion("LineDetail not like", value, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailIn(List<String> values) {
            addCriterion("LineDetail in", values, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailNotIn(List<String> values) {
            addCriterion("LineDetail not in", values, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailBetween(String value1, String value2) {
            addCriterion("LineDetail between", value1, value2, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinedetailNotBetween(String value1, String value2) {
            addCriterion("LineDetail not between", value1, value2, "linedetail");
            return (Criteria) this;
        }

        public Criteria andLinestateIsNull() {
            addCriterion("LineState is null");
            return (Criteria) this;
        }

        public Criteria andLinestateIsNotNull() {
            addCriterion("LineState is not null");
            return (Criteria) this;
        }

        public Criteria andLinestateEqualTo(String value) {
            addCriterion("LineState =", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateNotEqualTo(String value) {
            addCriterion("LineState <>", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateGreaterThan(String value) {
            addCriterion("LineState >", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateGreaterThanOrEqualTo(String value) {
            addCriterion("LineState >=", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateLessThan(String value) {
            addCriterion("LineState <", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateLessThanOrEqualTo(String value) {
            addCriterion("LineState <=", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateLike(String value) {
            addCriterion("LineState like", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateNotLike(String value) {
            addCriterion("LineState not like", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateIn(List<String> values) {
            addCriterion("LineState in", values, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateNotIn(List<String> values) {
            addCriterion("LineState not in", values, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateBetween(String value1, String value2) {
            addCriterion("LineState between", value1, value2, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateNotBetween(String value1, String value2) {
            addCriterion("LineState not between", value1, value2, "linestate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}