package com.zhao.po;

public class ItemInfo {
    private Integer itemId;

    private String itemname;

    private Float itemweight;

    private Float itemlenght;

    private Float itemwith;

    private Float itemheight;

    private Boolean itemstate;

    private String carId;
    
    private Integer orderId;

    private String itemdesc;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname == null ? null : itemname.trim();
    }

    public Float getItemweight() {
        return itemweight;
    }

    public void setItemweight(Float itemweight) {
        this.itemweight = itemweight;
    }

    public Float getItemlenght() {
        return itemlenght;
    }

    public void setItemlenght(Float itemlenght) {
        this.itemlenght = itemlenght;
    }

    public Float getItemwith() {
        return itemwith;
    }

    public void setItemwith(Float itemwith) {
        this.itemwith = itemwith;
    }

    public Float getItemheight() {
        return itemheight;
    }

    public void setItemheight(Float itemheight) {
        this.itemheight = itemheight;
    }

    public Boolean getItemstate() {
        return itemstate;
    }

    public void setItemstate(Boolean itemstate) {
        this.itemstate = itemstate;
    }


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getItemdesc() {
        return itemdesc;
    }

    public void setItemdesc(String itemdesc) {
        this.itemdesc = itemdesc == null ? null : itemdesc.trim();
    }

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		   this.carId = carId == null ? null : carId.trim();
	}

	@Override
	public String toString() {
		return "ItemInfo [itemId=" + itemId + ", itemname=" + itemname + ", itemweight=" + itemweight + ", itemlenght="
				+ itemlenght + ", itemwith=" + itemwith + ", itemheight=" + itemheight + ", itemstate=" + itemstate
				+ ", carId=" + carId + ", orderId=" + orderId + ", itemdesc=" + itemdesc + "]";
	}

    
    
}