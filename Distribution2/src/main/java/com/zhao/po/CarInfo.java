package com.zhao.po;

import java.util.ArrayList;
import java.util.List;

public class CarInfo {
    private String carid;

    private Boolean carstate;

    private Float maxweight;

    private Float carlength;

    private Float carwith;

    private Float carheight;

    private String notes;

//   这里定义 了items属性用来存放装载的商品
    private List<ItemInfo> items;
    
    
    
    public List<ItemInfo> getItems() {
		return items;
	}

	public void setItems(List<ItemInfo> items) {
		this.items = items;
	}

	public String getCarid() {
        return carid;
    }

    public void setCarid(String carid) {
        this.carid = carid == null ? null : carid.trim();
    }

    public Boolean getCarstate() {
        return carstate;
    }

    public void setCarstate(Boolean carstate) {
        this.carstate = carstate;
    }

    public Float getMaxweight() {
        return maxweight;
    }

    public void setMaxweight(Float maxweight) {
        this.maxweight = maxweight;
    }

    public Float getCarlength() {
        return carlength;
    }

    public void setCarlength(Float carlength) {
        this.carlength = carlength;
    }

    public Float getCarwith() {
        return carwith;
    }

    public void setCarwith(Float carwith) {
        this.carwith = carwith;
    }

    public Float getCarheight() {
        return carheight;
    }

    public void setCarheight(Float carheight) {
        this.carheight = carheight;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes == null ? null : notes.trim();
    }

	@Override
	public String toString() {
		return "CarInfo [carid=" + carid + ", carstate=" + carstate + ", maxweight=" + maxweight + ", carlength="
				+ carlength + ", carwith=" + carwith + ", carheight=" + carheight + ", notes=" + notes + "]";
	}
    
    
    
}