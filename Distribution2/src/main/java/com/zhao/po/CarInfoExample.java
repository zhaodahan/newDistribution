package com.zhao.po;

import java.util.ArrayList;
import java.util.List;

public class CarInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CarInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCaridIsNull() {
            addCriterion("carID is null");
            return (Criteria) this;
        }

        public Criteria andCaridIsNotNull() {
            addCriterion("carID is not null");
            return (Criteria) this;
        }

        public Criteria andCaridEqualTo(String value) {
            addCriterion("carID =", value, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridNotEqualTo(String value) {
            addCriterion("carID <>", value, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridGreaterThan(String value) {
            addCriterion("carID >", value, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridGreaterThanOrEqualTo(String value) {
            addCriterion("carID >=", value, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridLessThan(String value) {
            addCriterion("carID <", value, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridLessThanOrEqualTo(String value) {
            addCriterion("carID <=", value, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridLike(String value) {
            addCriterion("carID like", value, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridNotLike(String value) {
            addCriterion("carID not like", value, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridIn(List<String> values) {
            addCriterion("carID in", values, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridNotIn(List<String> values) {
            addCriterion("carID not in", values, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridBetween(String value1, String value2) {
            addCriterion("carID between", value1, value2, "carid");
            return (Criteria) this;
        }

        public Criteria andCaridNotBetween(String value1, String value2) {
            addCriterion("carID not between", value1, value2, "carid");
            return (Criteria) this;
        }

        public Criteria andCarstateIsNull() {
            addCriterion("carState is null");
            return (Criteria) this;
        }

        public Criteria andCarstateIsNotNull() {
            addCriterion("carState is not null");
            return (Criteria) this;
        }

        public Criteria andCarstateEqualTo(String value) {
            addCriterion("carState =", value, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateNotEqualTo(String value) {
            addCriterion("carState <>", value, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateGreaterThan(String value) {
            addCriterion("carState >", value, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateGreaterThanOrEqualTo(String value) {
            addCriterion("carState >=", value, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateLessThan(String value) {
            addCriterion("carState <", value, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateLessThanOrEqualTo(String value) {
            addCriterion("carState <=", value, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateLike(String value) {
            addCriterion("carState like", value, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateNotLike(String value) {
            addCriterion("carState not like", value, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateIn(List<String> values) {
            addCriterion("carState in", values, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateNotIn(List<String> values) {
            addCriterion("carState not in", values, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateBetween(String value1, String value2) {
            addCriterion("carState between", value1, value2, "carstate");
            return (Criteria) this;
        }

        public Criteria andCarstateNotBetween(String value1, String value2) {
            addCriterion("carState not between", value1, value2, "carstate");
            return (Criteria) this;
        }

        public Criteria andMaxweightIsNull() {
            addCriterion("maxWeight is null");
            return (Criteria) this;
        }

        public Criteria andMaxweightIsNotNull() {
            addCriterion("maxWeight is not null");
            return (Criteria) this;
        }

        public Criteria andMaxweightEqualTo(Float value) {
            addCriterion("maxWeight =", value, "maxweight");
            return (Criteria) this;
        }

        public Criteria andMaxweightNotEqualTo(Float value) {
            addCriterion("maxWeight <>", value, "maxweight");
            return (Criteria) this;
        }

        public Criteria andMaxweightGreaterThan(Float value) {
            addCriterion("maxWeight >", value, "maxweight");
            return (Criteria) this;
        }

        public Criteria andMaxweightGreaterThanOrEqualTo(Float value) {
            addCriterion("maxWeight >=", value, "maxweight");
            return (Criteria) this;
        }

        public Criteria andMaxweightLessThan(Float value) {
            addCriterion("maxWeight <", value, "maxweight");
            return (Criteria) this;
        }

        public Criteria andMaxweightLessThanOrEqualTo(Float value) {
            addCriterion("maxWeight <=", value, "maxweight");
            return (Criteria) this;
        }

        public Criteria andMaxweightIn(List<Float> values) {
            addCriterion("maxWeight in", values, "maxweight");
            return (Criteria) this;
        }

        public Criteria andMaxweightNotIn(List<Float> values) {
            addCriterion("maxWeight not in", values, "maxweight");
            return (Criteria) this;
        }

        public Criteria andMaxweightBetween(Float value1, Float value2) {
            addCriterion("maxWeight between", value1, value2, "maxweight");
            return (Criteria) this;
        }

        public Criteria andMaxweightNotBetween(Float value1, Float value2) {
            addCriterion("maxWeight not between", value1, value2, "maxweight");
            return (Criteria) this;
        }

        public Criteria andCarlengthIsNull() {
            addCriterion("carLength is null");
            return (Criteria) this;
        }

        public Criteria andCarlengthIsNotNull() {
            addCriterion("carLength is not null");
            return (Criteria) this;
        }

        public Criteria andCarlengthEqualTo(Float value) {
            addCriterion("carLength =", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthNotEqualTo(Float value) {
            addCriterion("carLength <>", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthGreaterThan(Float value) {
            addCriterion("carLength >", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthGreaterThanOrEqualTo(Float value) {
            addCriterion("carLength >=", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthLessThan(Float value) {
            addCriterion("carLength <", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthLessThanOrEqualTo(Float value) {
            addCriterion("carLength <=", value, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthIn(List<Float> values) {
            addCriterion("carLength in", values, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthNotIn(List<Float> values) {
            addCriterion("carLength not in", values, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthBetween(Float value1, Float value2) {
            addCriterion("carLength between", value1, value2, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarlengthNotBetween(Float value1, Float value2) {
            addCriterion("carLength not between", value1, value2, "carlength");
            return (Criteria) this;
        }

        public Criteria andCarwithIsNull() {
            addCriterion("carWith is null");
            return (Criteria) this;
        }

        public Criteria andCarwithIsNotNull() {
            addCriterion("carWith is not null");
            return (Criteria) this;
        }

        public Criteria andCarwithEqualTo(Float value) {
            addCriterion("carWith =", value, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarwithNotEqualTo(Float value) {
            addCriterion("carWith <>", value, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarwithGreaterThan(Float value) {
            addCriterion("carWith >", value, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarwithGreaterThanOrEqualTo(Float value) {
            addCriterion("carWith >=", value, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarwithLessThan(Float value) {
            addCriterion("carWith <", value, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarwithLessThanOrEqualTo(Float value) {
            addCriterion("carWith <=", value, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarwithIn(List<Float> values) {
            addCriterion("carWith in", values, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarwithNotIn(List<Float> values) {
            addCriterion("carWith not in", values, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarwithBetween(Float value1, Float value2) {
            addCriterion("carWith between", value1, value2, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarwithNotBetween(Float value1, Float value2) {
            addCriterion("carWith not between", value1, value2, "carwith");
            return (Criteria) this;
        }

        public Criteria andCarheightIsNull() {
            addCriterion("carheight is null");
            return (Criteria) this;
        }

        public Criteria andCarheightIsNotNull() {
            addCriterion("carheight is not null");
            return (Criteria) this;
        }

        public Criteria andCarheightEqualTo(Float value) {
            addCriterion("carheight =", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightNotEqualTo(Float value) {
            addCriterion("carheight <>", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightGreaterThan(Float value) {
            addCriterion("carheight >", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightGreaterThanOrEqualTo(Float value) {
            addCriterion("carheight >=", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightLessThan(Float value) {
            addCriterion("carheight <", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightLessThanOrEqualTo(Float value) {
            addCriterion("carheight <=", value, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightIn(List<Float> values) {
            addCriterion("carheight in", values, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightNotIn(List<Float> values) {
            addCriterion("carheight not in", values, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightBetween(Float value1, Float value2) {
            addCriterion("carheight between", value1, value2, "carheight");
            return (Criteria) this;
        }

        public Criteria andCarheightNotBetween(Float value1, Float value2) {
            addCriterion("carheight not between", value1, value2, "carheight");
            return (Criteria) this;
        }

        public Criteria andNotesIsNull() {
            addCriterion("notes is null");
            return (Criteria) this;
        }

        public Criteria andNotesIsNotNull() {
            addCriterion("notes is not null");
            return (Criteria) this;
        }

        public Criteria andNotesEqualTo(String value) {
            addCriterion("notes =", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotEqualTo(String value) {
            addCriterion("notes <>", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesGreaterThan(String value) {
            addCriterion("notes >", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesGreaterThanOrEqualTo(String value) {
            addCriterion("notes >=", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLessThan(String value) {
            addCriterion("notes <", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLessThanOrEqualTo(String value) {
            addCriterion("notes <=", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesLike(String value) {
            addCriterion("notes like", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotLike(String value) {
            addCriterion("notes not like", value, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesIn(List<String> values) {
            addCriterion("notes in", values, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotIn(List<String> values) {
            addCriterion("notes not in", values, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesBetween(String value1, String value2) {
            addCriterion("notes between", value1, value2, "notes");
            return (Criteria) this;
        }

        public Criteria andNotesNotBetween(String value1, String value2) {
            addCriterion("notes not between", value1, value2, "notes");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}