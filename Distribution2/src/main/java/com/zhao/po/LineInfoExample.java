package com.zhao.po;

import java.util.ArrayList;
import java.util.List;

public class LineInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public LineInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andLineidIsNull() {
            addCriterion("LineID is null");
            return (Criteria) this;
        }

        public Criteria andLineidIsNotNull() {
            addCriterion("LineID is not null");
            return (Criteria) this;
        }

        public Criteria andLineidEqualTo(Integer value) {
            addCriterion("LineID =", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidNotEqualTo(Integer value) {
            addCriterion("LineID <>", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidGreaterThan(Integer value) {
            addCriterion("LineID >", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidGreaterThanOrEqualTo(Integer value) {
            addCriterion("LineID >=", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidLessThan(Integer value) {
            addCriterion("LineID <", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidLessThanOrEqualTo(Integer value) {
            addCriterion("LineID <=", value, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidIn(List<Integer> values) {
            addCriterion("LineID in", values, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidNotIn(List<Integer> values) {
            addCriterion("LineID not in", values, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidBetween(Integer value1, Integer value2) {
            addCriterion("LineID between", value1, value2, "lineid");
            return (Criteria) this;
        }

        public Criteria andLineidNotBetween(Integer value1, Integer value2) {
            addCriterion("LineID not between", value1, value2, "lineid");
            return (Criteria) this;
        }

        public Criteria andLinestartIsNull() {
            addCriterion("LineStart is null");
            return (Criteria) this;
        }

        public Criteria andLinestartIsNotNull() {
            addCriterion("LineStart is not null");
            return (Criteria) this;
        }

        public Criteria andLinestartEqualTo(Integer value) {
            addCriterion("LineStart =", value, "linestart");
            return (Criteria) this;
        }

        public Criteria andLinestartNotEqualTo(Integer value) {
            addCriterion("LineStart <>", value, "linestart");
            return (Criteria) this;
        }

        public Criteria andLinestartGreaterThan(Integer value) {
            addCriterion("LineStart >", value, "linestart");
            return (Criteria) this;
        }

        public Criteria andLinestartGreaterThanOrEqualTo(Integer value) {
            addCriterion("LineStart >=", value, "linestart");
            return (Criteria) this;
        }

        public Criteria andLinestartLessThan(Integer value) {
            addCriterion("LineStart <", value, "linestart");
            return (Criteria) this;
        }

        public Criteria andLinestartLessThanOrEqualTo(Integer value) {
            addCriterion("LineStart <=", value, "linestart");
            return (Criteria) this;
        }

        public Criteria andLinestartIn(List<Integer> values) {
            addCriterion("LineStart in", values, "linestart");
            return (Criteria) this;
        }

        public Criteria andLinestartNotIn(List<Integer> values) {
            addCriterion("LineStart not in", values, "linestart");
            return (Criteria) this;
        }

        public Criteria andLinestartBetween(Integer value1, Integer value2) {
            addCriterion("LineStart between", value1, value2, "linestart");
            return (Criteria) this;
        }

        public Criteria andLinestartNotBetween(Integer value1, Integer value2) {
            addCriterion("LineStart not between", value1, value2, "linestart");
            return (Criteria) this;
        }

        public Criteria andLineendIsNull() {
            addCriterion("LineEnd is null");
            return (Criteria) this;
        }

        public Criteria andLineendIsNotNull() {
            addCriterion("LineEnd is not null");
            return (Criteria) this;
        }

        public Criteria andLineendEqualTo(Integer value) {
            addCriterion("LineEnd =", value, "lineend");
            return (Criteria) this;
        }

        public Criteria andLineendNotEqualTo(Integer value) {
            addCriterion("LineEnd <>", value, "lineend");
            return (Criteria) this;
        }

        public Criteria andLineendGreaterThan(Integer value) {
            addCriterion("LineEnd >", value, "lineend");
            return (Criteria) this;
        }

        public Criteria andLineendGreaterThanOrEqualTo(Integer value) {
            addCriterion("LineEnd >=", value, "lineend");
            return (Criteria) this;
        }

        public Criteria andLineendLessThan(Integer value) {
            addCriterion("LineEnd <", value, "lineend");
            return (Criteria) this;
        }

        public Criteria andLineendLessThanOrEqualTo(Integer value) {
            addCriterion("LineEnd <=", value, "lineend");
            return (Criteria) this;
        }

        public Criteria andLineendIn(List<Integer> values) {
            addCriterion("LineEnd in", values, "lineend");
            return (Criteria) this;
        }

        public Criteria andLineendNotIn(List<Integer> values) {
            addCriterion("LineEnd not in", values, "lineend");
            return (Criteria) this;
        }

        public Criteria andLineendBetween(Integer value1, Integer value2) {
            addCriterion("LineEnd between", value1, value2, "lineend");
            return (Criteria) this;
        }

        public Criteria andLineendNotBetween(Integer value1, Integer value2) {
            addCriterion("LineEnd not between", value1, value2, "lineend");
            return (Criteria) this;
        }

        public Criteria andLinelengthIsNull() {
            addCriterion("LineLength is null");
            return (Criteria) this;
        }

        public Criteria andLinelengthIsNotNull() {
            addCriterion("LineLength is not null");
            return (Criteria) this;
        }

        public Criteria andLinelengthEqualTo(Float value) {
            addCriterion("LineLength =", value, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinelengthNotEqualTo(Float value) {
            addCriterion("LineLength <>", value, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinelengthGreaterThan(Float value) {
            addCriterion("LineLength >", value, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinelengthGreaterThanOrEqualTo(Float value) {
            addCriterion("LineLength >=", value, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinelengthLessThan(Float value) {
            addCriterion("LineLength <", value, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinelengthLessThanOrEqualTo(Float value) {
            addCriterion("LineLength <=", value, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinelengthIn(List<Float> values) {
            addCriterion("LineLength in", values, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinelengthNotIn(List<Float> values) {
            addCriterion("LineLength not in", values, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinelengthBetween(Float value1, Float value2) {
            addCriterion("LineLength between", value1, value2, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinelengthNotBetween(Float value1, Float value2) {
            addCriterion("LineLength not between", value1, value2, "linelength");
            return (Criteria) this;
        }

        public Criteria andLinecostIsNull() {
            addCriterion("LineCost is null");
            return (Criteria) this;
        }

        public Criteria andLinecostIsNotNull() {
            addCriterion("LineCost is not null");
            return (Criteria) this;
        }

        public Criteria andLinecostEqualTo(Float value) {
            addCriterion("LineCost =", value, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinecostNotEqualTo(Float value) {
            addCriterion("LineCost <>", value, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinecostGreaterThan(Float value) {
            addCriterion("LineCost >", value, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinecostGreaterThanOrEqualTo(Float value) {
            addCriterion("LineCost >=", value, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinecostLessThan(Float value) {
            addCriterion("LineCost <", value, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinecostLessThanOrEqualTo(Float value) {
            addCriterion("LineCost <=", value, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinecostIn(List<Float> values) {
            addCriterion("LineCost in", values, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinecostNotIn(List<Float> values) {
            addCriterion("LineCost not in", values, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinecostBetween(Float value1, Float value2) {
            addCriterion("LineCost between", value1, value2, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinecostNotBetween(Float value1, Float value2) {
            addCriterion("LineCost not between", value1, value2, "linecost");
            return (Criteria) this;
        }

        public Criteria andLinetimeIsNull() {
            addCriterion("LineTime is null");
            return (Criteria) this;
        }

        public Criteria andLinetimeIsNotNull() {
            addCriterion("LineTime is not null");
            return (Criteria) this;
        }

        public Criteria andLinetimeEqualTo(Integer value) {
            addCriterion("LineTime =", value, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinetimeNotEqualTo(Integer value) {
            addCriterion("LineTime <>", value, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinetimeGreaterThan(Integer value) {
            addCriterion("LineTime >", value, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinetimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("LineTime >=", value, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinetimeLessThan(Integer value) {
            addCriterion("LineTime <", value, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinetimeLessThanOrEqualTo(Integer value) {
            addCriterion("LineTime <=", value, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinetimeIn(List<Integer> values) {
            addCriterion("LineTime in", values, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinetimeNotIn(List<Integer> values) {
            addCriterion("LineTime not in", values, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinetimeBetween(Integer value1, Integer value2) {
            addCriterion("LineTime between", value1, value2, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinetimeNotBetween(Integer value1, Integer value2) {
            addCriterion("LineTime not between", value1, value2, "linetime");
            return (Criteria) this;
        }

        public Criteria andLinestateIsNull() {
            addCriterion("LineState is null");
            return (Criteria) this;
        }

        public Criteria andLinestateIsNotNull() {
            addCriterion("LineState is not null");
            return (Criteria) this;
        }

        public Criteria andLinestateEqualTo(String value) {
            addCriterion("LineState =", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateNotEqualTo(String value) {
            addCriterion("LineState <>", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateGreaterThan(String value) {
            addCriterion("LineState >", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateGreaterThanOrEqualTo(String value) {
            addCriterion("LineState >=", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateLessThan(String value) {
            addCriterion("LineState <", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateLessThanOrEqualTo(String value) {
            addCriterion("LineState <=", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateLike(String value) {
            addCriterion("LineState like", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateNotLike(String value) {
            addCriterion("LineState not like", value, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateIn(List<String> values) {
            addCriterion("LineState in", values, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateNotIn(List<String> values) {
            addCriterion("LineState not in", values, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateBetween(String value1, String value2) {
            addCriterion("LineState between", value1, value2, "linestate");
            return (Criteria) this;
        }

        public Criteria andLinestateNotBetween(String value1, String value2) {
            addCriterion("LineState not between", value1, value2, "linestate");
            return (Criteria) this;
        }

        public Criteria andIschangeIsNull() {
            addCriterion("IsChange is null");
            return (Criteria) this;
        }

        public Criteria andIschangeIsNotNull() {
            addCriterion("IsChange is not null");
            return (Criteria) this;
        }

        public Criteria andIschangeEqualTo(String value) {
            addCriterion("IsChange =", value, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeNotEqualTo(String value) {
            addCriterion("IsChange <>", value, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeGreaterThan(String value) {
            addCriterion("IsChange >", value, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeGreaterThanOrEqualTo(String value) {
            addCriterion("IsChange >=", value, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeLessThan(String value) {
            addCriterion("IsChange <", value, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeLessThanOrEqualTo(String value) {
            addCriterion("IsChange <=", value, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeLike(String value) {
            addCriterion("IsChange like", value, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeNotLike(String value) {
            addCriterion("IsChange not like", value, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeIn(List<String> values) {
            addCriterion("IsChange in", values, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeNotIn(List<String> values) {
            addCriterion("IsChange not in", values, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeBetween(String value1, String value2) {
            addCriterion("IsChange between", value1, value2, "ischange");
            return (Criteria) this;
        }

        public Criteria andIschangeNotBetween(String value1, String value2) {
            addCriterion("IsChange not between", value1, value2, "ischange");
            return (Criteria) this;
        }

        public Criteria andLinedescIsNull() {
            addCriterion("LineDesc is null");
            return (Criteria) this;
        }

        public Criteria andLinedescIsNotNull() {
            addCriterion("LineDesc is not null");
            return (Criteria) this;
        }

        public Criteria andLinedescEqualTo(String value) {
            addCriterion("LineDesc =", value, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescNotEqualTo(String value) {
            addCriterion("LineDesc <>", value, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescGreaterThan(String value) {
            addCriterion("LineDesc >", value, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescGreaterThanOrEqualTo(String value) {
            addCriterion("LineDesc >=", value, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescLessThan(String value) {
            addCriterion("LineDesc <", value, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescLessThanOrEqualTo(String value) {
            addCriterion("LineDesc <=", value, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescLike(String value) {
            addCriterion("LineDesc like", value, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescNotLike(String value) {
            addCriterion("LineDesc not like", value, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescIn(List<String> values) {
            addCriterion("LineDesc in", values, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescNotIn(List<String> values) {
            addCriterion("LineDesc not in", values, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescBetween(String value1, String value2) {
            addCriterion("LineDesc between", value1, value2, "linedesc");
            return (Criteria) this;
        }

        public Criteria andLinedescNotBetween(String value1, String value2) {
            addCriterion("LineDesc not between", value1, value2, "linedesc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}