package com.zhao.po;

import java.util.ArrayList;
import java.util.List;

public class ItemInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ItemInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andItemIdIsNull() {
            addCriterion("item_id is null");
            return (Criteria) this;
        }

        public Criteria andItemIdIsNotNull() {
            addCriterion("item_id is not null");
            return (Criteria) this;
        }

        public Criteria andItemIdEqualTo(Integer value) {
            addCriterion("item_id =", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotEqualTo(Integer value) {
            addCriterion("item_id <>", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdGreaterThan(Integer value) {
            addCriterion("item_id >", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("item_id >=", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdLessThan(Integer value) {
            addCriterion("item_id <", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdLessThanOrEqualTo(Integer value) {
            addCriterion("item_id <=", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdIn(List<Integer> values) {
            addCriterion("item_id in", values, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotIn(List<Integer> values) {
            addCriterion("item_id not in", values, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdBetween(Integer value1, Integer value2) {
            addCriterion("item_id between", value1, value2, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotBetween(Integer value1, Integer value2) {
            addCriterion("item_id not between", value1, value2, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemnameIsNull() {
            addCriterion("itemName is null");
            return (Criteria) this;
        }

        public Criteria andItemnameIsNotNull() {
            addCriterion("itemName is not null");
            return (Criteria) this;
        }

        public Criteria andItemnameEqualTo(String value) {
            addCriterion("itemName =", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameNotEqualTo(String value) {
            addCriterion("itemName <>", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameGreaterThan(String value) {
            addCriterion("itemName >", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameGreaterThanOrEqualTo(String value) {
            addCriterion("itemName >=", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameLessThan(String value) {
            addCriterion("itemName <", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameLessThanOrEqualTo(String value) {
            addCriterion("itemName <=", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameLike(String value) {
            addCriterion("itemName like", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameNotLike(String value) {
            addCriterion("itemName not like", value, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameIn(List<String> values) {
            addCriterion("itemName in", values, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameNotIn(List<String> values) {
            addCriterion("itemName not in", values, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameBetween(String value1, String value2) {
            addCriterion("itemName between", value1, value2, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemnameNotBetween(String value1, String value2) {
            addCriterion("itemName not between", value1, value2, "itemname");
            return (Criteria) this;
        }

        public Criteria andItemweightIsNull() {
            addCriterion("itemWeight is null");
            return (Criteria) this;
        }

        public Criteria andItemweightIsNotNull() {
            addCriterion("itemWeight is not null");
            return (Criteria) this;
        }

        public Criteria andItemweightEqualTo(Float value) {
            addCriterion("itemWeight =", value, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemweightNotEqualTo(Float value) {
            addCriterion("itemWeight <>", value, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemweightGreaterThan(Float value) {
            addCriterion("itemWeight >", value, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemweightGreaterThanOrEqualTo(Float value) {
            addCriterion("itemWeight >=", value, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemweightLessThan(Float value) {
            addCriterion("itemWeight <", value, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemweightLessThanOrEqualTo(Float value) {
            addCriterion("itemWeight <=", value, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemweightIn(List<Float> values) {
            addCriterion("itemWeight in", values, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemweightNotIn(List<Float> values) {
            addCriterion("itemWeight not in", values, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemweightBetween(Float value1, Float value2) {
            addCriterion("itemWeight between", value1, value2, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemweightNotBetween(Float value1, Float value2) {
            addCriterion("itemWeight not between", value1, value2, "itemweight");
            return (Criteria) this;
        }

        public Criteria andItemlenghtIsNull() {
            addCriterion("itemLenght is null");
            return (Criteria) this;
        }

        public Criteria andItemlenghtIsNotNull() {
            addCriterion("itemLenght is not null");
            return (Criteria) this;
        }

        public Criteria andItemlenghtEqualTo(Float value) {
            addCriterion("itemLenght =", value, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemlenghtNotEqualTo(Float value) {
            addCriterion("itemLenght <>", value, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemlenghtGreaterThan(Float value) {
            addCriterion("itemLenght >", value, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemlenghtGreaterThanOrEqualTo(Float value) {
            addCriterion("itemLenght >=", value, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemlenghtLessThan(Float value) {
            addCriterion("itemLenght <", value, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemlenghtLessThanOrEqualTo(Float value) {
            addCriterion("itemLenght <=", value, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemlenghtIn(List<Float> values) {
            addCriterion("itemLenght in", values, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemlenghtNotIn(List<Float> values) {
            addCriterion("itemLenght not in", values, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemlenghtBetween(Float value1, Float value2) {
            addCriterion("itemLenght between", value1, value2, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemlenghtNotBetween(Float value1, Float value2) {
            addCriterion("itemLenght not between", value1, value2, "itemlenght");
            return (Criteria) this;
        }

        public Criteria andItemwithIsNull() {
            addCriterion("itemWith is null");
            return (Criteria) this;
        }

        public Criteria andItemwithIsNotNull() {
            addCriterion("itemWith is not null");
            return (Criteria) this;
        }

        public Criteria andItemwithEqualTo(Float value) {
            addCriterion("itemWith =", value, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemwithNotEqualTo(Float value) {
            addCriterion("itemWith <>", value, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemwithGreaterThan(Float value) {
            addCriterion("itemWith >", value, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemwithGreaterThanOrEqualTo(Float value) {
            addCriterion("itemWith >=", value, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemwithLessThan(Float value) {
            addCriterion("itemWith <", value, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemwithLessThanOrEqualTo(Float value) {
            addCriterion("itemWith <=", value, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemwithIn(List<Float> values) {
            addCriterion("itemWith in", values, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemwithNotIn(List<Float> values) {
            addCriterion("itemWith not in", values, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemwithBetween(Float value1, Float value2) {
            addCriterion("itemWith between", value1, value2, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemwithNotBetween(Float value1, Float value2) {
            addCriterion("itemWith not between", value1, value2, "itemwith");
            return (Criteria) this;
        }

        public Criteria andItemheightIsNull() {
            addCriterion("itemHeight is null");
            return (Criteria) this;
        }

        public Criteria andItemheightIsNotNull() {
            addCriterion("itemHeight is not null");
            return (Criteria) this;
        }

        public Criteria andItemheightEqualTo(Float value) {
            addCriterion("itemHeight =", value, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemheightNotEqualTo(Float value) {
            addCriterion("itemHeight <>", value, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemheightGreaterThan(Float value) {
            addCriterion("itemHeight >", value, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemheightGreaterThanOrEqualTo(Float value) {
            addCriterion("itemHeight >=", value, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemheightLessThan(Float value) {
            addCriterion("itemHeight <", value, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemheightLessThanOrEqualTo(Float value) {
            addCriterion("itemHeight <=", value, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemheightIn(List<Float> values) {
            addCriterion("itemHeight in", values, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemheightNotIn(List<Float> values) {
            addCriterion("itemHeight not in", values, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemheightBetween(Float value1, Float value2) {
            addCriterion("itemHeight between", value1, value2, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemheightNotBetween(Float value1, Float value2) {
            addCriterion("itemHeight not between", value1, value2, "itemheight");
            return (Criteria) this;
        }

        public Criteria andItemstateIsNull() {
            addCriterion("itemState is null");
            return (Criteria) this;
        }

        public Criteria andItemstateIsNotNull() {
            addCriterion("itemState is not null");
            return (Criteria) this;
        }

        public Criteria andItemstateEqualTo(String value) {
            addCriterion("itemState =", value, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateNotEqualTo(String value) {
            addCriterion("itemState <>", value, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateGreaterThan(String value) {
            addCriterion("itemState >", value, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateGreaterThanOrEqualTo(String value) {
            addCriterion("itemState >=", value, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateLessThan(String value) {
            addCriterion("itemState <", value, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateLessThanOrEqualTo(String value) {
            addCriterion("itemState <=", value, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateLike(String value) {
            addCriterion("itemState like", value, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateNotLike(String value) {
            addCriterion("itemState not like", value, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateIn(List<String> values) {
            addCriterion("itemState in", values, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateNotIn(List<String> values) {
            addCriterion("itemState not in", values, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateBetween(String value1, String value2) {
            addCriterion("itemState between", value1, value2, "itemstate");
            return (Criteria) this;
        }

        public Criteria andItemstateNotBetween(String value1, String value2) {
            addCriterion("itemState not between", value1, value2, "itemstate");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Integer value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Integer value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Integer value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Integer value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Integer> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Integer> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andCarIdIsNull() {
            addCriterion("car_id is null");
            return (Criteria) this;
        }

        public Criteria andCarIdIsNotNull() {
            addCriterion("car_id is not null");
            return (Criteria) this;
        }

        public Criteria andCarIdEqualTo(String value) {
            addCriterion("car_id =", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotEqualTo(String value) {
            addCriterion("car_id <>", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdGreaterThan(String value) {
            addCriterion("car_id >", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdGreaterThanOrEqualTo(String value) {
            addCriterion("car_id >=", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdLessThan(String value) {
            addCriterion("car_id <", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdLessThanOrEqualTo(String value) {
            addCriterion("car_id <=", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdLike(String value) {
            addCriterion("car_id like", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotLike(String value) {
            addCriterion("car_id not like", value, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdIn(List<String> values) {
            addCriterion("car_id in", values, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotIn(List<String> values) {
            addCriterion("car_id not in", values, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdBetween(String value1, String value2) {
            addCriterion("car_id between", value1, value2, "carId");
            return (Criteria) this;
        }

        public Criteria andCarIdNotBetween(String value1, String value2) {
            addCriterion("car_id not between", value1, value2, "carId");
            return (Criteria) this;
        }

        public Criteria andItemdescIsNull() {
            addCriterion("itemDesc is null");
            return (Criteria) this;
        }

        public Criteria andItemdescIsNotNull() {
            addCriterion("itemDesc is not null");
            return (Criteria) this;
        }

        public Criteria andItemdescEqualTo(String value) {
            addCriterion("itemDesc =", value, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescNotEqualTo(String value) {
            addCriterion("itemDesc <>", value, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescGreaterThan(String value) {
            addCriterion("itemDesc >", value, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescGreaterThanOrEqualTo(String value) {
            addCriterion("itemDesc >=", value, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescLessThan(String value) {
            addCriterion("itemDesc <", value, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescLessThanOrEqualTo(String value) {
            addCriterion("itemDesc <=", value, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescLike(String value) {
            addCriterion("itemDesc like", value, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescNotLike(String value) {
            addCriterion("itemDesc not like", value, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescIn(List<String> values) {
            addCriterion("itemDesc in", values, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescNotIn(List<String> values) {
            addCriterion("itemDesc not in", values, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescBetween(String value1, String value2) {
            addCriterion("itemDesc between", value1, value2, "itemdesc");
            return (Criteria) this;
        }

        public Criteria andItemdescNotBetween(String value1, String value2) {
            addCriterion("itemDesc not between", value1, value2, "itemdesc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}