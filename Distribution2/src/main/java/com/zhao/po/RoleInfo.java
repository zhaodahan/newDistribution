package com.zhao.po;

public class RoleInfo {
    private Integer roleid;

    private String rolename;

    private Integer rolerightid;

    private String roledese;

//  这里定义了一个权限变量，来用来在查询角色的时候一并将其权限给查询出来
    private RightInfo right;
    
    
    public RightInfo getRight() {
		return right;
	}

	public void setRight(RightInfo right) {
		this.right = right;
	}

	public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename == null ? null : rolename.trim();
    }

    public Integer getRolerightid() {
        return rolerightid;
    }

    public void setRolerightid(Integer rolerightid) {
        this.rolerightid = rolerightid;
    }

    public String getRoledese() {
        return roledese;
    }

    public void setRoledese(String roledese) {
        this.roledese = roledese == null ? null : roledese.trim();
    }

	@Override
	public String toString() {
		return "RoleInfo [roleid=" + roleid + ", rolename=" + rolename + ", rolerightid=" + rolerightid + ", roledese="
				+ roledese + ", right=" + right + "]";
	}
    
    
}