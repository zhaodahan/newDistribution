package com.zhao.po;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Pattern;

public class OrderInfo {
    private Integer orderId;

    private String receivername;

    @Pattern(regexp="(/^[1][3,4,5,7,8][0-9]{9}$/)"
    		,message="11位有效的电话号码")
    private String receiverphone;

    private String receivercity;

    private String receiverdistrict;

    private String receiveraddress;

    private Boolean isdealstate;

    private Date createdate;

    private Integer customerId;


//  查询订单的时候关联查询到订单中存在的商品
    private List<ItemInfo> items;
    
    public List<ItemInfo> getItems() {
		return items;
	}

	public void setItems(List<ItemInfo> items) {
		this.items = items;
	}
    
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getReceivername() {
        return receivername;
    }

    public void setReceivername(String receivername) {
        this.receivername = receivername == null ? null : receivername.trim();
    }

    public String getReceiverphone() {
        return receiverphone;
    }

    public void setReceiverphone(String receiverphone) {
        this.receiverphone = receiverphone == null ? null : receiverphone.trim();
    }

    public String getReceivercity() {
        return receivercity;
    }

    public void setReceivercity(String receivercity) {
        this.receivercity = receivercity == null ? null : receivercity.trim();
    }

    public String getReceiverdistrict() {
        return receiverdistrict;
    }

    public void setReceiverdistrict(String receiverdistrict) {
        this.receiverdistrict = receiverdistrict == null ? null : receiverdistrict.trim();
    }

    public String getReceiveraddress() {
        return receiveraddress;
    }

    public void setReceiveraddress(String receiveraddress) {
        this.receiveraddress = receiveraddress == null ? null : receiveraddress.trim();
    }

    public Boolean getIsdealstate() {
        return isdealstate;
    }

    public void setIsdealstate(Boolean isdealstate) {
        this.isdealstate = isdealstate;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

	@Override
	public String toString() {
		return "OrderInfo [orderId=" + orderId + ", receivername=" + receivername + ", receiverphone=" + receiverphone
				+ ", receivercity=" + receivercity + ", receiverdistrict=" + receiverdistrict + ", receiveraddress="
				+ receiveraddress + ", isdealstate=" + isdealstate + ", createdate=" + createdate + ", customerId="
				+ customerId + ", items=" + items + "]";
	}


    
    
}