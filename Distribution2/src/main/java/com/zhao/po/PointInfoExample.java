package com.zhao.po;

import java.util.ArrayList;
import java.util.List;

public class PointInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PointInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPointidIsNull() {
            addCriterion("pointID is null");
            return (Criteria) this;
        }

        public Criteria andPointidIsNotNull() {
            addCriterion("pointID is not null");
            return (Criteria) this;
        }

        public Criteria andPointidEqualTo(Integer value) {
            addCriterion("pointID =", value, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointidNotEqualTo(Integer value) {
            addCriterion("pointID <>", value, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointidGreaterThan(Integer value) {
            addCriterion("pointID >", value, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointidGreaterThanOrEqualTo(Integer value) {
            addCriterion("pointID >=", value, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointidLessThan(Integer value) {
            addCriterion("pointID <", value, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointidLessThanOrEqualTo(Integer value) {
            addCriterion("pointID <=", value, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointidIn(List<Integer> values) {
            addCriterion("pointID in", values, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointidNotIn(List<Integer> values) {
            addCriterion("pointID not in", values, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointidBetween(Integer value1, Integer value2) {
            addCriterion("pointID between", value1, value2, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointidNotBetween(Integer value1, Integer value2) {
            addCriterion("pointID not between", value1, value2, "pointid");
            return (Criteria) this;
        }

        public Criteria andPointnameIsNull() {
            addCriterion("pointName is null");
            return (Criteria) this;
        }

        public Criteria andPointnameIsNotNull() {
            addCriterion("pointName is not null");
            return (Criteria) this;
        }

        public Criteria andPointnameEqualTo(String value) {
            addCriterion("pointName =", value, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameNotEqualTo(String value) {
            addCriterion("pointName <>", value, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameGreaterThan(String value) {
            addCriterion("pointName >", value, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameGreaterThanOrEqualTo(String value) {
            addCriterion("pointName >=", value, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameLessThan(String value) {
            addCriterion("pointName <", value, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameLessThanOrEqualTo(String value) {
            addCriterion("pointName <=", value, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameLike(String value) {
            addCriterion("pointName like", value, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameNotLike(String value) {
            addCriterion("pointName not like", value, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameIn(List<String> values) {
            addCriterion("pointName in", values, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameNotIn(List<String> values) {
            addCriterion("pointName not in", values, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameBetween(String value1, String value2) {
            addCriterion("pointName between", value1, value2, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointnameNotBetween(String value1, String value2) {
            addCriterion("pointName not between", value1, value2, "pointname");
            return (Criteria) this;
        }

        public Criteria andPointcityIsNull() {
            addCriterion("pointCity is null");
            return (Criteria) this;
        }

        public Criteria andPointcityIsNotNull() {
            addCriterion("pointCity is not null");
            return (Criteria) this;
        }

        public Criteria andPointcityEqualTo(String value) {
            addCriterion("pointCity =", value, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityNotEqualTo(String value) {
            addCriterion("pointCity <>", value, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityGreaterThan(String value) {
            addCriterion("pointCity >", value, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityGreaterThanOrEqualTo(String value) {
            addCriterion("pointCity >=", value, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityLessThan(String value) {
            addCriterion("pointCity <", value, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityLessThanOrEqualTo(String value) {
            addCriterion("pointCity <=", value, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityLike(String value) {
            addCriterion("pointCity like", value, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityNotLike(String value) {
            addCriterion("pointCity not like", value, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityIn(List<String> values) {
            addCriterion("pointCity in", values, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityNotIn(List<String> values) {
            addCriterion("pointCity not in", values, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityBetween(String value1, String value2) {
            addCriterion("pointCity between", value1, value2, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointcityNotBetween(String value1, String value2) {
            addCriterion("pointCity not between", value1, value2, "pointcity");
            return (Criteria) this;
        }

        public Criteria andPointdistrictIsNull() {
            addCriterion("pointDistrict is null");
            return (Criteria) this;
        }

        public Criteria andPointdistrictIsNotNull() {
            addCriterion("pointDistrict is not null");
            return (Criteria) this;
        }

        public Criteria andPointdistrictEqualTo(String value) {
            addCriterion("pointDistrict =", value, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictNotEqualTo(String value) {
            addCriterion("pointDistrict <>", value, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictGreaterThan(String value) {
            addCriterion("pointDistrict >", value, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictGreaterThanOrEqualTo(String value) {
            addCriterion("pointDistrict >=", value, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictLessThan(String value) {
            addCriterion("pointDistrict <", value, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictLessThanOrEqualTo(String value) {
            addCriterion("pointDistrict <=", value, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictLike(String value) {
            addCriterion("pointDistrict like", value, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictNotLike(String value) {
            addCriterion("pointDistrict not like", value, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictIn(List<String> values) {
            addCriterion("pointDistrict in", values, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictNotIn(List<String> values) {
            addCriterion("pointDistrict not in", values, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictBetween(String value1, String value2) {
            addCriterion("pointDistrict between", value1, value2, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointdistrictNotBetween(String value1, String value2) {
            addCriterion("pointDistrict not between", value1, value2, "pointdistrict");
            return (Criteria) this;
        }

        public Criteria andPointadressIsNull() {
            addCriterion("pointAdress is null");
            return (Criteria) this;
        }

        public Criteria andPointadressIsNotNull() {
            addCriterion("pointAdress is not null");
            return (Criteria) this;
        }

        public Criteria andPointadressEqualTo(String value) {
            addCriterion("pointAdress =", value, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressNotEqualTo(String value) {
            addCriterion("pointAdress <>", value, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressGreaterThan(String value) {
            addCriterion("pointAdress >", value, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressGreaterThanOrEqualTo(String value) {
            addCriterion("pointAdress >=", value, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressLessThan(String value) {
            addCriterion("pointAdress <", value, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressLessThanOrEqualTo(String value) {
            addCriterion("pointAdress <=", value, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressLike(String value) {
            addCriterion("pointAdress like", value, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressNotLike(String value) {
            addCriterion("pointAdress not like", value, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressIn(List<String> values) {
            addCriterion("pointAdress in", values, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressNotIn(List<String> values) {
            addCriterion("pointAdress not in", values, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressBetween(String value1, String value2) {
            addCriterion("pointAdress between", value1, value2, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointadressNotBetween(String value1, String value2) {
            addCriterion("pointAdress not between", value1, value2, "pointadress");
            return (Criteria) this;
        }

        public Criteria andPointdescIsNull() {
            addCriterion("pointDesc is null");
            return (Criteria) this;
        }

        public Criteria andPointdescIsNotNull() {
            addCriterion("pointDesc is not null");
            return (Criteria) this;
        }

        public Criteria andPointdescEqualTo(String value) {
            addCriterion("pointDesc =", value, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescNotEqualTo(String value) {
            addCriterion("pointDesc <>", value, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescGreaterThan(String value) {
            addCriterion("pointDesc >", value, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescGreaterThanOrEqualTo(String value) {
            addCriterion("pointDesc >=", value, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescLessThan(String value) {
            addCriterion("pointDesc <", value, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescLessThanOrEqualTo(String value) {
            addCriterion("pointDesc <=", value, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescLike(String value) {
            addCriterion("pointDesc like", value, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescNotLike(String value) {
            addCriterion("pointDesc not like", value, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescIn(List<String> values) {
            addCriterion("pointDesc in", values, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescNotIn(List<String> values) {
            addCriterion("pointDesc not in", values, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescBetween(String value1, String value2) {
            addCriterion("pointDesc between", value1, value2, "pointdesc");
            return (Criteria) this;
        }

        public Criteria andPointdescNotBetween(String value1, String value2) {
            addCriterion("pointDesc not between", value1, value2, "pointdesc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}