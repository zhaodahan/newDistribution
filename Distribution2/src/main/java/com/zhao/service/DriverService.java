package com.zhao.service;

import java.util.List;

import com.zhao.po.DriverCarInfo;

/**
 * @author 作者 E-zhao:
 * @version 创建时间：2017年11月18日 下午4:57:04 类说明
 */
public interface DriverService {

	/**
	 * 查询所有的驾驶者信息
	 * 
	 * @return
	 */
	List<DriverCarInfo> getAll();

	/**
	 * 添加驾驶员
	 * 
	 * @param driverCarInfo
	 */
	void add(DriverCarInfo driverCarInfo);

	/**
	 * 在进行更新请求的时候,根据id来查询
	 * 
	 * @param id
	 * @return
	 */
	DriverCarInfo getdriver(Integer driverid);

	/**
	 * 根据传入的对象更新
	 * 
	 * @param driver
	 */
	void updatedriver(DriverCarInfo driver);

	void deletedriver(List<Integer> del_ids);

	void deletedriver(Integer id);
}
