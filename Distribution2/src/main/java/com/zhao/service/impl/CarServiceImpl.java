package com.zhao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.CarInfoMapper;
import com.zhao.dao.ItemInfoMapper;
import com.zhao.po.CarInfo;
import com.zhao.po.CarInfoExample;
import com.zhao.po.CarInfoExample.Criteria;
import com.zhao.po.ItemInfo;
import com.zhao.service.CarService;
import com.zhao.utils.BackPackageUtil;
import com.zhao.utils.ListConvertAdapter;
import com.zhao.utils.SerialNumber;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarInfoMapper carInfoMapper;

	@Autowired
	private ItemInfoMapper itemInfoMapper;

	/**
	 * 查询所有的车辆
	 * 
	 * @return
	 */
	public List<CarInfo> getAll() {
		// TODO Auto-generated method stub
		return carInfoMapper.selectByExample(null);
	}

	/**
	 * 添加车
	 * 
	 * @param carInfo
	 */
	public void add(CarInfo carInfo) {
		// TODO Auto-generated method stub
		if (carInfo != null) {
			// 这里让车牌号 流水自增
			carInfo.setCarid(SerialNumber.createSerial("zhao", 6));
			carInfoMapper.insertSelective(carInfo);
		}
	}

	/**
	 * 在进行更新请求的时候
	 * 
	 * @param id
	 * @return
	 */
	public CarInfo getCar(String id) {
		// TODO Auto-generated method stub
		return carInfoMapper.selectByPrimaryKey(id);
	}

	/**
	 * 更新车辆信息
	 * 
	 * @param car
	 */
	public void updateCar(CarInfo car) {
		// 进行有选择的更新
		carInfoMapper.updateByPrimaryKeySelective(car);
	}

	/**
	 * 批量删除车辆信息
	 * 
	 * @param del_ids
	 */
	public void deleteCar(List<String> del_ids) {
		// TODO Auto-generated method stub
		CarInfoExample carInfoExample = new CarInfoExample();
		Criteria carCriteria = carInfoExample.createCriteria();
		carCriteria.andCaridIn(del_ids);
		carInfoMapper.deleteByExample(carInfoExample);
	}

	/**
	 * 根据id来单独删除 这里需要注意的是车辆的id是字符串
	 * 
	 * @param id
	 */
	public void deleteCar(String ids) {
		// TODO Auto-generated method stub
		carInfoMapper.deleteByPrimaryKey(ids);
	}

	/**
	 * 从我们库存的车中找出一辆
	 * 
	 * @return
	 */
	public CarInfo findCar() {
		// TODO Auto-generated method stub
		// 从库存中找出可用的车的集合，然后从中随机选出一个；
		CarInfoExample carInfoExample = new CarInfoExample();
		Criteria carCriteria = carInfoExample.createCriteria();
		// 选出车辆可用的车的集合
		carCriteria.andCarstateEqualTo("Y");
		List<CarInfo> cars = carInfoMapper.selectByExampleWithItem(carInfoExample);

		// 这里应该是随机的从集合中选择一个元素.取第一个？

		return cars != null ? cars.get(0) : null;
	}

	


	/**
	 * 车辆装货：将商品货物装上车，并修改商品的状态为已经拣货 这里是批量拣货，一次拣一个集合的货
	 * 
	 * 将商品装车思路： 1. 使用背包算法获得，能够装车的item集合 2. 调用dao层方法更新item表中的carid。 3.
	 * 从总的Item集合中去掉已经装车的item 4. 如果还有没有装车的商品，重新分配车进行装车
	 * 
	 * @param car
	 * @param items
	 */

	public void pickItem(CarInfo car, List<ItemInfo> items) {
		// 这里需要判断一下所有商品是否能够一次性的装完，如果不能一次性的装完，就再获取一次车再装一次，直至装完
		do {

			List<ItemInfo> fillList = new BackPackageUtil(car, items).backpack();

			// 通过工具类ListConvertAdapter 获取集合中的元素属性的集合
			List<Integer> itemIds = new ListConvertAdapter<Integer, ItemInfo>(fillList, "itemId").getElements();
			// 这里通过背包算法来筛选出能放入车中的商品
			if (itemIds != null) {
				itemInfoMapper.updateCarIDByItemIDs(car.getCarid(), itemIds);
			}

			items.removeAll(fillList);
			if (items.size() > 0) {
				// 商品没有装完，还需要车
				car = ChangeCar();
			}

		} while (items.size() > 0);

	}

	private CarInfo ChangeCar() {
		// TODO Auto-generated method stub
		return findCar();
	}

	/**
	 * 这个方法用来修改
	 * 
	 * @param car
	 */
	public void ChangeCarState(CarInfo car) {
		carInfoMapper.updateByPrimaryKeySelective(car);
	}

}
