package com.zhao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.DriverCarInfoMapper;
import com.zhao.po.DriverCarInfo;
import com.zhao.po.DriverCarInfoExample;
import com.zhao.po.DriverCarInfoExample.Criteria;
import com.zhao.service.DriverService;


@Service
public class DriverServiceImpl implements DriverService{

	@Autowired
	private DriverCarInfoMapper driverCarInfoMapper;
	
	/**
	 * 查询所有的驾驶者信息
	 * @return
	 */
	public List<DriverCarInfo> getAll() {
		// TODO Auto-generated method stub
		return driverCarInfoMapper.selectByExample(null);
	}

	/**
	 * 添加驾驶员
	 * @param driverCarInfo
	 */
	public void add(DriverCarInfo driverCarInfo) {
		// TODO Auto-generated method stub
		driverCarInfoMapper.insertSelective(driverCarInfo);
	}

	/**
	 * 在进行更新请求的时候,根据id来查询
	 * @param id
	 * @return
	 */
	public DriverCarInfo getdriver(Integer driverid) {
		// TODO Auto-generated method stub
		return driverCarInfoMapper.selectByPrimaryKey(driverid);
	}


	/**
	 * 根据传入的对象更新
	 * @param driver
	 */
	public void updatedriver(DriverCarInfo driver) {
		// TODO Auto-generated method stub
		driverCarInfoMapper.updateByPrimaryKeySelective(driver);
	}

	public void deletedriver(List<Integer> del_ids) {
		// TODO Auto-generated method stub
		DriverCarInfoExample driverExample=new DriverCarInfoExample();
		Criteria driverCriteria=driverExample.createCriteria();
		driverCriteria.andDriveridIn(del_ids);
		driverCarInfoMapper.deleteByExample(driverExample);
		
	}

	public void deletedriver(Integer id) {
		// TODO Auto-generated method stub
		driverCarInfoMapper.deleteByPrimaryKey(id);
	}
}
