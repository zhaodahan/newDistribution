package com.zhao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.PointInfoMapper;
import com.zhao.po.PointInfo;
import com.zhao.service.PointService;

/**
 * @author 作者 E-mail:
 * @version 创建时间：2017年11月22日 上午11:32:59 类说明
 */
@Service
public class PointServiceImpl implements PointService {

	@Autowired
	private PointInfoMapper pointInfoMapper;

	/**
	 * 获取所有的节点
	 */
	@Override
	public List<PointInfo> getPoints() {
		// TODO Auto-generated method stub
		return pointInfoMapper.selectByExample(null);
	}

	/**
	 * 添加地址节点
	 */
	@Override
	public void addPoint(PointInfo point) {
		// TODO 这里添加地址节点需要判断这个地址节点是否已经存在了
		// 如果这个节点存在就不报错
		if (!IsHasPoint(point)) {
			pointInfoMapper.insertSelective(point);
		}

	}

	/**
	 * 判断这个节点是否存在
	 * 
	 * @param point
	 * @return
	 */
	private boolean IsHasPoint(PointInfo point) {
		// 比较除了pointid以外的其他属性
		return pointInfoMapper.isHasPoint(point);
	}

}
