package com.zhao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.OrderInfoMapper;
import com.zhao.po.CarInfo;
import com.zhao.po.ItemInfo;
import com.zhao.po.OrderInfo;
import com.zhao.service.CarService;
import com.zhao.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderInfoMapper orderInfoMapper;

	@Autowired
	private CarService carService;

	/**
	 * 查询列表
	 * 
	 * @return
	 */
	public List<OrderInfo> getAll() {
		// TODO Auto-generated method stub
		return orderInfoMapper.selectByExample(null);
	}

	/**
	 * 添加订单
	 * 
	 * @param orderInfo
	 */
	public void add(OrderInfo orderInfo) {
		// TODO Auto-generated method stub
		orderInfoMapper.insertSelective(orderInfo);
	}

	/**
	 * 更新订单
	 * 
	 * @param order
	 */
	public void updateorder(OrderInfo order) {
		// TODO Auto-generated method stub
		orderInfoMapper.updateByPrimaryKey(order);
	}

	/**
	 * 根据id来进行查询
	 * 
	 * @param orderid
	 * @return
	 */
	public OrderInfo getorder(Integer orderid) {
		// TODO Auto-generated method stub
		return orderInfoMapper.selectByPrimaryKeyWithItem(orderid);
	}

	/**
	 * 这个方法是用来处理订单的
	 * 
	 * @param parseInt
	 */
	public Boolean dealOrder(int id) {
		// TODO Auto-generated method stub
		// 1.先根据id获得需要处理的订单的详细信息
		OrderInfo orderInfo = getorder(id);
		return sortPick(orderInfo);

	}

	/**
	 * 分配拣货
	 * 
	 * @param orderInfo
	 */

	@Override
	public void pickOrder(int orderid) {
		// TODO Auto-generated method stub
		sortPick(getorder(orderid));
	}

	private Boolean sortPick(OrderInfo orderInfo) {
		// 1.获取订单的商品集合
		List<ItemInfo> items = orderInfo.getItems();

		// 2.获取可用车辆
		CarInfo car = carService.findCar();
		if (car != null) {
			// 有可用的车辆,就让车辆装货 。是一次装上所有的货物吗
			carService.pickItem(car, items);
			return true;
		}

		return false;
	}

}
