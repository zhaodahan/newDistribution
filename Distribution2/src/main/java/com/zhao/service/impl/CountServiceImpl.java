package com.zhao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhao.dao.CarInfoMapper;
import com.zhao.po.CarInfo;
import com.zhao.service.CountService;

/** 
* @author 作者 E-mail: 
* @version 创建时间：2017年11月22日 下午4:34:55 
* 类说明 
*/

@Service
public class CountServiceImpl implements CountService {

	@Autowired
	private CarInfoMapper carInfoMapper;
	/**
	 * 这里统计车辆配送（体积 重量 件数 距离）
	 */
	@Override
	public void countAllCar() {
	//这里 统计的话，那肯定需要取出所有满足条件的car数据
       
	}
	
	
	/**
	 * 这里统计一辆车: 那肯定是要传入
	 * 这里是传入carid，还是传入一个car对象，前台应该传回的是一个id
	 * 统计了需要返回什么信息？
	 * 返回的配送的车辆配送货物的（体积 重量 件数 距离）
	 * 这些信息怎么存储？怎么返回了？
	 */
	@Override
	public void countOneCar(String carid) {
	//这里 统计的话，那肯定需要取出所有满足条件的car数据
//       1.根据id取出我们要统计的car
		CarInfo car=carInfoMapper.selectByPrimaryKey(carid);
	    		
	}

}
 