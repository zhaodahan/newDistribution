package com.zhao.service;

import java.util.List;

import com.zhao.po.PointInfo;

/**
 * @author 作者 E-mail:
 * @version 创建时间：2017年11月22日 上午11:23:42 类说明
 */
public interface PointService {

	/**
	 * 获取所有的point
	 * 
	 * @return
	 */
	List<PointInfo> getPoints();

	/**
	 * 添加地址节点
	 * 
	 * @param point
	 */
	void addPoint(PointInfo point);
			
}
