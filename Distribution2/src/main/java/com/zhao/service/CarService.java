package com.zhao.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.zhao.dao.CarInfoMapper;
import com.zhao.dao.ItemInfoMapper;
import com.zhao.po.CarInfo;
import com.zhao.po.CarInfoExample;
import com.zhao.po.ItemInfo;
import com.zhao.po.CarInfoExample.Criteria;
import com.zhao.utils.BackPackageUtil;
import com.zhao.utils.SerialNumber;

/**
 * @author 作者 zhao:
 * @version 创建时间：2017年11月18日 下午4:51:56 类说明
 */
public interface CarService {

	/**
	 * 查询所有的车辆
	 * 
	 * @return
	 */
	List<CarInfo> getAll();

	/**
	 * 添加车
	 * 
	 * @param carInfo
	 */
	void add(CarInfo carInfo);

	/**
	 * 在进行更新请求的时候
	 * 
	 * @param id
	 * @return
	 */
	CarInfo getCar(String id);

	/**
	 * 更新车辆信息
	 * 
	 * @param car
	 */
	void updateCar(CarInfo car);

	/**
	 * 批量删除车辆信息
	 * 
	 * @param del_ids
	 */
	void deleteCar(List<String> del_ids);

	/**
	 * 根据id来单独删除 这里需要注意的是车辆的id是字符串
	 * 
	 * @param id
	 */
	void deleteCar(String ids);

	/**
	 * 从我们库存的车中找出一辆
	 * 
	 * @return
	 */
	CarInfo findCar();


	/**
	 * 车辆装货：将商品货物装上车，并修改商品的状态为已经拣货 这里是批量拣货，一次拣一个集合的货
	 * 
	 * @param car
	 * @param items
	 */

	void pickItem(CarInfo car, List<ItemInfo> items);

	/**
	 * 这个方法用来修改
	 * 
	 * @param car
	 */
	void ChangeCarState(CarInfo car);
}
