package com.zhao.service;

import java.util.List;

import com.zhao.po.CarInfo;
import com.zhao.po.ItemInfo;
import com.zhao.po.OrderInfo;

/**
 * @author 作者 E-mail:
 * @version 创建时间：2017年11月18日 下午5:06:33 类说明
 */
public interface OrderService {
	/**
	 * 查询列表
	 * 
	 * @return
	 */
	List<OrderInfo> getAll();

	/**
	 * 添加订单
	 * 
	 * @param orderInfo
	 */
	void add(OrderInfo orderInfo);

	/**
	 * 更新订单
	 * 
	 * @param order
	 */
	void updateorder(OrderInfo order);

	/**
	 * 根据id来进行查询
	 * 
	 * @param orderid
	 * @return
	 */
	OrderInfo getorder(Integer orderid);

	/**
	 * 这个方法是用来处理订单的
	 * 
	 * @param parseInt
	 */
	Boolean dealOrder(int id);

	/**
	 * 分配拣货
	 * 
	 * @param orderInfo
	 */
	void pickOrder(int orderid);
	
}
