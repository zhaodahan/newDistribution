package com.zhao.service;

import com.zhao.exception.ErrorAddressException;
import com.zhao.exception.ErrorLineException;
import com.zhao.po.LineDetail;
import com.zhao.po.LineInfo;
import com.zhao.po.OrderInfo;

/**
 * @author 作者 E-mail:
 * @version 创建时间：2017年11月18日 下午5:03:23 类说明
 */
public interface LineService {

	/**
	 * 提供选择最短路径的服务
	 * 
	 * @param start
	 * @param order
	 * @return
	 * @throws ErrorAddressException
	 */
	LineDetail choseLine(String start, OrderInfo order) throws ErrorAddressException;

	/**
	 * 修改路线的权重
	 * 
	 * @param line
	 */
	void updateLine(LineInfo line);

	/**
	 * 添加路线
	 * 
	 * @param line
	 * @throws ErrorLineException
	 */
	void addLine(LineInfo line) throws ErrorLineException;

}
