package com.zhao.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zhao.po.Msg;
import com.zhao.po.PointInfo;
import com.zhao.service.PointService;

/**
 * 这个action是用来操作地址节点的
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/point")
public class PointController {

	@Autowired
	private PointService pointService;

	/**
	 * 这个方法用来用来在下拉框的时候获取一下我们选择的添加路线的时候获取所有的开始点或者结束点
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getPoints")
	@ResponseBody
	public Msg getPoints(HttpServletRequest request) throws Exception{
        List<PointInfo> points=pointService.getPoints(); 
		return Msg.success().add("points", points);

	}
	
	
	/**
	 * 添加地址节点
	 * 
	 * @return
	 */
	@RequestMapping(value="/addPoint",method=RequestMethod.POST)
	@ResponseBody
	public Msg addPoint(PointInfo point)throws Exception {
        pointService.addPoint(point);
		return Msg.success();
	}
	

}
