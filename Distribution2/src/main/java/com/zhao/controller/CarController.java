package com.zhao.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhao.po.CarInfo;
import com.zhao.po.Msg;
import com.zhao.service.CarService;
import com.zhao.utils.CommonUtils;

@Controller
@RequestMapping("/car")
public class CarController {

	@Autowired
	private CarService carService;
	// ==================先写简单的CRUD=================

	/**
	 * 这个方法是用来返回字符串，只针对浏览器的
	 * @param pn
	 * @return
	 */
//	@RequestMapping("/getcars")
	public String getCars(@RequestParam(value = "pn", defaultValue = "1") Integer pn,Model model)throws Exception {
		// 这本不是一个分页查询，但是为了实现分页查询，在这里我们 引入了PageHelper插件
		PageHelper.startPage(pn, CommonUtils.PAGESIZE);
		List<CarInfo> cars = carService.getAll();
		// 将我们查询的数据分装成pageInfo对象
		PageInfo<CarInfo> page = new PageInfo<CarInfo>(cars, CommonUtils.PAGESIZE);
		model.addAttribute("page", page);
		return "carlist.jsp";

	}

	/**
	 * 返回json数据可以提高扩展性。(不是浏览器也可以解析，其他平台类似Android) 这里要设想到： 这里的查询是涉及到分页的
	 * 
	 * @return
	 */
	@RequestMapping("/getcars")
	@ResponseBody
	public Msg getCars(@RequestParam(value = "pn", defaultValue = "1") Integer pn) throws Exception{
		// 这本不是一个分页查询，但是为了实现分页查询，在这里我们 引入了PageHelper插件
		PageHelper.startPage(pn, CommonUtils.PAGESIZE);
		List<CarInfo> cars = carService.getAll();
		// 将我们查询的数据分装成pageInfo对象
		PageInfo<CarInfo> page = new PageInfo<CarInfo>(cars, CommonUtils.PAGESIZE);
		return Msg.success().add("page", page);

	}

	/**
	 * 车辆添加
	 * 
	 * @return
	 */
	@RequestMapping(value="/addcar",method=RequestMethod.POST)
	@ResponseBody
	public Msg addCar(CarInfo car)throws Exception {
		carService.add(car);
		return Msg.success();
	}

	
	/**
	 * 下面完成车辆信息的修改功能
	 * 修改的逻辑是： 
	 * 先查询一个，并将其回显在我们的表格之中，然后将将修改的进行更新
	 */
	
	/**
	 * 根据id查询车辆，用来信息表单回显
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/getCar/{id}",method=RequestMethod.GET)
	@ResponseBody
	public Msg getCar(@PathVariable("id")String  id)throws Exception{	
		CarInfo car=carService.getCar(id);	
		return Msg.success().add("car", car);
	}
	
	
	/**
	 * 更行车辆信息
	 * 在这里进行需要请求方式是PUT，他在页面的表达方式是post请求+传送过来的数据加上“_method=put”
	 * 不过，Spring提供了HttpPutFormContentFilter，让我们可以在Ajax中直接发送PUT请求 
	 * @param car
	 * @return
	 */
	@RequestMapping(value="/updateCar/{carId}",method=RequestMethod.PUT)
	@ResponseBody
	public Msg updateCar(CarInfo car)throws Exception{ 
		carService.updateCar(car);
 		return Msg.success();
	}
	
	
	/**
	 * 这个方法是删除
	 * 单个批量二合一
	 * 批量删除：1-2-3(前端使用—来连接id)
	 * 单个删除：1
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/deleteCar/{ids}",method=RequestMethod.DELETE)
	@ResponseBody
	public Msg deleteCar(@PathVariable("ids")String ids)throws Exception{
		//批量删除
		if(ids.contains("-")){
			List<String> del_ids = new ArrayList<>();
			String[] str_ids = ids.split("-");
			//组装id的集合
			for (String id : str_ids) {
				del_ids.add(id);
			}
			carService.deleteCar(del_ids);
			
		}else{
			carService.deleteCar(ids);
		}
		return Msg.success();
	}
	
	
	
	
}
