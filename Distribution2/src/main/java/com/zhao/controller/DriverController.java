package com.zhao.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhao.po.DriverCarInfo;
import com.zhao.po.Msg;
import com.zhao.service.DriverService;
import com.zhao.utils.CommonUtils;

@Controller
@RequestMapping("/driver")
public class DriverController {

	@Autowired
	private DriverService driverService;
	// ==================先写简单的CRUD=================

	/**
	 * 这个方法是用来返回字符串，只针对浏览器的
	 * 
	 * @param pn
	 * @return
	 */
	// @RequestMapping("/getdrivers")
	public String getdrivers(@RequestParam(value = "pn", defaultValue = "1") Integer pn, Model model) throws Exception {
		// 这本不是一个分页查询，但是为了实现分页查询，在这里我们 引入了PageHelper插件
		PageHelper.startPage(pn, CommonUtils.PAGESIZE);
		List<DriverCarInfo> drivers = driverService.getAll();
		// 将我们查询的数据分装成pageInfo对象
		PageInfo<DriverCarInfo> page = new PageInfo<DriverCarInfo>(drivers, CommonUtils.PAGESIZE);
		model.addAttribute("page", page);
		return "driverlist.jsp";

	}

	/**
	 * 返回json数据可以提高扩展性。(不是浏览器也可以解析，其他平台类似Android) 这里要设想到： 这里的查询是涉及到分页的
	 * 
	 * @return
	 */
	@RequestMapping("/getdrivers")
	@ResponseBody
	public Msg getdrivers(@RequestParam(value = "pn", defaultValue = "1") Integer pn) throws Exception {
		// 这本不是一个分页查询，但是为了实现分页查询，在这里我们 引入了PageHelper插件
		PageHelper.startPage(pn, CommonUtils.PAGESIZE);
		List<DriverCarInfo> drivers = driverService.getAll();
		// 将我们查询的数据分装成pageInfo对象
		PageInfo<DriverCarInfo> page = new PageInfo<DriverCarInfo>(drivers, CommonUtils.PAGESIZE);
		return Msg.success().add("page", page);

	}

	/**
	 * 驾驶员添加
	 * 这里使用了JSR303校验
	 * @return
	 */
	@RequestMapping(value = "/adddriver", method = RequestMethod.POST)
	@ResponseBody
	// 这里加上 @RequestBody 是在测试的时候，我们传递过来的是json字符串
	public Msg adddriver(@Valid DriverCarInfo driver, BindingResult result) throws Exception {
		if (result.hasErrors()) {
			// 校验失败
			Map<String, Object> map = new HashMap<>();
			List<FieldError> errors = result.getFieldErrors();
			for (FieldError fieldError : errors) {
				System.out.println("错误的字段名：" + fieldError.getField());
				System.out.println("错误信息：" + fieldError.getDefaultMessage());
				map.put(fieldError.getField(), fieldError.getDefaultMessage());
			}
			return Msg.fail().add("errorFields", map);
		}
		else {
			driverService.add(driver);
			return Msg.success();
		}

	}
	
	
	/**
	 * 更行驾驶员信息 在这里进行需要请求方式是PUT，他在页面的表达方式是post请求+传送过来的数据加上“_method=put”
	 * 不过，Spring提供了HttpPutFormContentFilter，让我们可以在Ajax中直接发送PUT请求
	 * 
	 * @param driver
	 * @return
	 */
	@RequestMapping(value = "/updatedriver/{driverId}", method = RequestMethod.PUT)
	@ResponseBody
	public Msg updatedriver(@Valid DriverCarInfo driver, BindingResult result) throws Exception {
		if (result.hasErrors()) {
			// 校验失败
			Map<String, Object> map = new HashMap<>();
			List<FieldError> errors = result.getFieldErrors();
			for (FieldError fieldError : errors) {
				System.out.println("错误的字段名：" + fieldError.getField());
				System.out.println("错误信息：" + fieldError.getDefaultMessage());
				map.put(fieldError.getField(), fieldError.getDefaultMessage());
			}
			return Msg.fail().add("errorFields", map);
		}
		else {
			driverService.updatedriver(driver);
			return Msg.success();
		}


	}


	/**
	 * 下面完成驾驶员信息的修改功能 修改的逻辑是： 先查询一个，并将其回显在我们的表格之中，然后将将修改的进行更新
	 */

	/**
	 * 根据id查询驾驶员，用来信息表单回显
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getdriver/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Msg getdriver(@PathVariable("id") String id) throws Exception {
		Integer driverid = Integer.parseInt(id);
		DriverCarInfo driver = driverService.getdriver(driverid);
		return Msg.success().add("driver", driver);
	}


	/**
	 * 这个方法是删除 单个批量二合一 批量删除：1-2-3(前端使用—来连接id) 单个删除：1
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deletedriver/{ids}", method = RequestMethod.DELETE)
	@ResponseBody
	public Msg deletedriver(@PathVariable("ids") String ids) throws Exception {
		// 批量删除
		if (ids.contains("-")) {
			List<Integer> del_ids = new ArrayList<>();
			String[] str_ids = ids.split("-");
			// 组装id的集合
			for (String id : str_ids) {
				del_ids.add(Integer.parseInt(id));
			}
			driverService.deletedriver(del_ids);

		} else {
			Integer id = Integer.parseInt(ids);
			driverService.deletedriver(id);
		}
		return Msg.success();
	}

}
