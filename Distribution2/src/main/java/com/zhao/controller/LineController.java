package com.zhao.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zhao.exception.ErrorAddressException;
import com.zhao.exception.ErrorLineException;
import com.zhao.po.LineDetail;
import com.zhao.po.LineInfo;
import com.zhao.po.Msg;
import com.zhao.po.OrderInfo;
import com.zhao.service.LineService;
import com.zhao.utils.LogUtil;

@Controller
@RequestMapping("/line")
public class LineController {

	@Autowired
	private LineService lineService;

	/**
	 * 这个方法用来获取最短路径.传入起点和终点
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getLine")
	@ResponseBody
	public Msg getlines(String start, OrderInfo order, HttpServletRequest request) {

		LineDetail line;
		try {
			line = lineService.choseLine(start, order);
			return line != null ? Msg.success().add("line", line) : Msg.success();
		} catch (ErrorAddressException e) {
			LogUtil.addErrorLog("选择路线", "LineController.getlines", null, request, e);
		}
		return Msg.fail().add("error", "获取路径错误");
	}

	/**
	 * 修改路线的权值(这里修改必须要有路线的id)
	 * 
	 * @param line
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateLine/{lineid}", method = RequestMethod.PUT)
	@ResponseBody
	public Msg updateLine(LineInfo line) throws Exception {

		lineService.updateLine(line);
		return Msg.success();

	}

	/**
	 * 添加路线： 需要传进来的肯定是一个LineInfo对象
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addLine", method = RequestMethod.POST)
	@ResponseBody
	// 这里加上 @RequestBody 是在测试的时候，我们传递过来的是json字符串
	public Msg addLine(LineInfo line, HttpServletRequest request){
		try {
			lineService.addLine(line);
			return Msg.success();
		} catch (ErrorLineException e) {
			// TODO Auto-generated catch block
			LogUtil.addErrorLog("选择路线", "LineController.getlines", null, request, e);
			return Msg.fail();
		}

	}

}
