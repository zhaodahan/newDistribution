package com.zhao.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zhao.exception.ErrorAddressException;
import com.zhao.po.LineDetail;
import com.zhao.po.Msg;
import com.zhao.service.CountService;
import com.zhao.utils.LogUtil;

/**
 * 这个action的创建是用来满足统计的
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/count")
public class CountController {

	@Autowired 
	private CountService countService;
	/**
	 * 这个方法用来统计e g: 车辆配送（体积 重量 件数 距离）
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/countCar")
	@ResponseBody
	public Msg countCar(HttpServletRequest request) {
		// 这里怎么进行统计？返回什么? 需不需要传递什么参数？
		countService.countAllCar();
		return Msg.success();
	}

}
