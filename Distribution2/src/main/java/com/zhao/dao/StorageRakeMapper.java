package com.zhao.dao;

import com.zhao.po.StorageRake;
import com.zhao.po.StorageRakeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface StorageRakeMapper {
    long countByExample(StorageRakeExample example);

    int deleteByExample(StorageRakeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(StorageRake record);

    int insertSelective(StorageRake record);

    List<StorageRake> selectByExample(StorageRakeExample example);

    StorageRake selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") StorageRake record, @Param("example") StorageRakeExample example);

    int updateByExample(@Param("record") StorageRake record, @Param("example") StorageRakeExample example);

    int updateByPrimaryKeySelective(StorageRake record);

    int updateByPrimaryKey(StorageRake record);
}