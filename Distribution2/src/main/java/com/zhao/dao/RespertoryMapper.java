package com.zhao.dao;

import com.zhao.po.Respertory;
import com.zhao.po.RespertoryExample;
import java.util.List;

public interface RespertoryMapper {
    long countByExample(RespertoryExample example);

    List<Respertory> selectByExample(RespertoryExample example);

    Respertory selectByPrimaryKey(Integer repertoryid);
}