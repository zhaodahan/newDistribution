package com.zhao.dao;

import com.zhao.po.ItemInfo;
import com.zhao.po.ItemInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ItemInfoMapper {
    long countByExample(ItemInfoExample example);

    int deleteByExample(ItemInfoExample example);

    int deleteByPrimaryKey(Integer itemId);

    int insert(ItemInfo record);

    int insertSelective(ItemInfo record);

    List<ItemInfo> selectByExample(ItemInfoExample example);
  

    ItemInfo selectByPrimaryKey(Integer itemId);
    
    List<ItemInfo> selectByOrderKey(Integer orderId);
    
    List<ItemInfo> selectByCarId(String carid);
    
    int updateByExampleSelective(@Param("record") ItemInfo record, @Param("example") ItemInfoExample example);

    int updateByExample(@Param("record") ItemInfo record, @Param("example") ItemInfoExample example);

    int updateByPrimaryKeySelective(ItemInfo record);

    int updateByPrimaryKey(ItemInfo record);

    
	int updateCarIDByItemID(@Param("carid")String carid,  @Param("itemId") Integer itemId);
    
    /**
     * 更改items集合的的carid    
     * @param carid
     * @param itemIds
     * @return
     */
	int updateCarIDByItemIDs(@Param("carid")String carid,  @Param("itemIds")List<Integer> itemIds);
}