package com.zhao.dao;

import com.zhao.po.PointInfo;
import com.zhao.po.PointInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PointInfoMapper {
	long countByExample(PointInfoExample example);

	int deleteByExample(PointInfoExample example);

	int deleteByPrimaryKey(Integer pointid);

	int insert(PointInfo record);

	int insertSelective(PointInfo record);

	List<PointInfo> selectByExample(PointInfoExample example);

	PointInfo selectByPrimaryKey(Integer pointid);

	int updateByExampleSelective(@Param("record") PointInfo record, @Param("example") PointInfoExample example);

	int updateByExample(@Param("record") PointInfo record, @Param("example") PointInfoExample example);

	int updateByPrimaryKeySelective(PointInfo record);

	int updateByPrimaryKey(PointInfo record);

	/**
	 * 这个方法是用来判断是否存在 
	 * Mybatis是根据查询到的记录数进行转换的(1=true,0=false)
	 * 需要注意的地方：如果查询到多条记录(大于1)，返回的却是false
	 * 
	 * @param point
	 * @return
	 */
	boolean isHasPoint(PointInfo point);

}