package com.zhao.dao;

import com.zhao.po.LineDetail;
import com.zhao.po.LineDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LineDetailMapper {
    long countByExample(LineDetailExample example);

    int deleteByExample(LineDetailExample example);

    int deleteByPrimaryKey(Integer lineid);

    int insert(LineDetail record);

    int insertSelective(LineDetail record);

    List<LineDetail> selectByExample(LineDetailExample example);

    LineDetail selectByPrimaryKey(Integer lineid);

    int updateByExampleSelective(@Param("record") LineDetail record, @Param("example") LineDetailExample example);

    int updateByExample(@Param("record") LineDetail record, @Param("example") LineDetailExample example);

    int updateByPrimaryKeySelective(LineDetail record);

    int updateByPrimaryKey(LineDetail record);

    /**
     * 查询是否存在满足要求的记录
     * @param startedPoint
     * @param endPoint
     * @return
     */
	LineDetail getMinRoute(@Param("start")int startedPoint, @Param("end")int endPoint);
}