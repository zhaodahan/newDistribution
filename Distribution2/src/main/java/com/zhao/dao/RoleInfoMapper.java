package com.zhao.dao;

import com.zhao.po.RoleInfo;
import com.zhao.po.RoleInfoExample;
import java.util.List;

public interface RoleInfoMapper {
    long countByExample(RoleInfoExample example);

    List<RoleInfo> selectByExample(RoleInfoExample example);

    RoleInfo selectByPrimaryKey(Integer roleid);
    
    List<RoleInfo> selectByExampleWithRight(RoleInfoExample example);

    RoleInfo selectByPrimaryKeyWithRight(Integer roleid);
}