package com.zhao.dao;

import com.zhao.po.RightInfo;
import com.zhao.po.RightInfoExample;
import java.util.List;

public interface RightInfoMapper {
    long countByExample(RightInfoExample example);

    List<RightInfo> selectByExample(RightInfoExample example);

    RightInfo selectByPrimaryKey(Integer rightid);
}