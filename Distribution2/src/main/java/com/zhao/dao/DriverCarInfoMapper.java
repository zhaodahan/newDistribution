package com.zhao.dao;

import com.zhao.po.DriverCarInfo;
import com.zhao.po.DriverCarInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DriverCarInfoMapper {
    long countByExample(DriverCarInfoExample example);

    int deleteByExample(DriverCarInfoExample example);

    int deleteByPrimaryKey(Integer driverid);

    int insert(DriverCarInfo record);

    int insertSelective(DriverCarInfo record);

    List<DriverCarInfo> selectByExample(DriverCarInfoExample example);

    DriverCarInfo selectByPrimaryKey(Integer driverid);
    
    List<DriverCarInfo> selectByExampleWithRoleRight(DriverCarInfoExample example);

    DriverCarInfo selectByPrimaryKeyWithRoleRight(Integer driverid);

    int updateByExampleSelective(@Param("record") DriverCarInfo record, @Param("example") DriverCarInfoExample example);

    int updateByExample(@Param("record") DriverCarInfo record, @Param("example") DriverCarInfoExample example);

    int updateByPrimaryKeySelective(DriverCarInfo record);

    int updateByPrimaryKey(DriverCarInfo record);
}