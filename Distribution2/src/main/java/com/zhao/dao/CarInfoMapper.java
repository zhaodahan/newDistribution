package com.zhao.dao;

import com.zhao.po.CarInfo;
import com.zhao.po.CarInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CarInfoMapper {
    long countByExample(CarInfoExample example);

    int deleteByExample(CarInfoExample example);

    int deleteByPrimaryKey(String carid);

    int insert(CarInfo record);

    int insertSelective(CarInfo record);

    List<CarInfo> selectByExample(CarInfoExample example);

    CarInfo selectByPrimaryKey(String carid);
    
    List<CarInfo> selectByExampleWithItem(CarInfoExample example);

    CarInfo selectByPrimaryKeyWithItem(String carid);
    

    int updateByExampleSelective(@Param("record") CarInfo record, @Param("example") CarInfoExample example);

    int updateByExample(@Param("record") CarInfo record, @Param("example") CarInfoExample example);

    int updateByPrimaryKeySelective(CarInfo record);

    int updateByPrimaryKey(CarInfo record);
    
}