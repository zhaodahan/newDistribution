package com.zhao.dao;

import com.zhao.po.LineInfo;
import com.zhao.po.LineInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LineInfoMapper {
    long countByExample(LineInfoExample example);

    int deleteByExample(LineInfoExample example);

    int deleteByPrimaryKey(Integer lineid);

    int insert(LineInfo record);

    int insertSelective(LineInfo record);

    List<LineInfo> selectByExample(LineInfoExample example);

    LineInfo selectByPrimaryKey(Integer lineid);

    int updateByExampleSelective(@Param("record") LineInfo record, @Param("example") LineInfoExample example);

    int updateByExample(@Param("record") LineInfo record, @Param("example") LineInfoExample example);

    int updateByPrimaryKeySelective(LineInfo record);

    int updateByPrimaryKey(LineInfo record);
}