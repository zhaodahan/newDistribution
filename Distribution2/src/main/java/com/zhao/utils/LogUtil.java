package com.zhao.utils; 
/** 
* @author 作者 E-mail: 
* @version 创建时间：2017年11月20日 下午5:09:43 
* 类说明 
*/

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import net.sf.json.JSONObject;



public class LogUtil {

    private static final Logger logger = Logger.getLogger(LogUtil.class);

    public static void main(String[] args) {

	try{
		/*List<String> ss = null;
		ss.add("1");*/
		FileInputStream f = new FileInputStream(new File(""));
		System.out.println(f.toString());
	}catch(Exception e ){
		addErrorLog("订单确认页面初始化", "confirmIndex", null, null, e);
	}
	
    }
    


    //

    /**
     * 方法日志
     * 
     * @param operatingBusiness
     *            操作业务
     * @param methodName
     *            方法名
     * @param parameter
     *            对象类参数
     * @param request
     *            request类参数
  
     * @param returnParameter
     *            方法返回值
     */
    public static void addInfoLog(String operatingBusiness, String methodName, Object parameter,
	    HttpServletRequest request,Object returnParameter) {
	HashMap<String, Object> resMap = getParameter(parameter, request);
	logger.info(",方法业务描述：" + operatingBusiness + ",类+方法名：" + methodName
		+ ",传入参数---：" + resMap + ",返回参数：" + returnParameter);
    }

    private static HashMap<String, Object> getParameter(Object parameter, HttpServletRequest request) {
	HashMap<String, Object> resMap = new HashMap<String, Object>();
	if(parameter instanceof String && StringUtils.isNotEmpty(String.valueOf(parameter))){
	    resMap.put("字符串类参数",parameter);
	}else if (parameter != null && !(parameter instanceof String) ) {
	    resMap.put("对象类参数", JSONObject.fromObject(parameter));
	}
	if (request != null) {
	    Enumeration<?> rnames = request.getParameterNames();
	    HashMap<String, Object> RequestMap = new HashMap<String, Object>();
	    for (Enumeration<?> e = rnames; e.hasMoreElements();) {
		String thisName = e.nextElement().toString();
		String thisValue = request.getParameter(thisName);
		RequestMap.put(thisName, thisValue);
	    }
	    if (RequestMap.size() > 0) {
		resMap.put("Request类参数", RequestMap);
	    }
	}
	return resMap;
    }

    /**
     * 过程日志
     * 
     * @param operatingBusiness
     *            操作业务
     * @param methodName
     *            方法名
     * @param operatorUser
     *            操作人
     */
    public static void addInfoLog(String operatingBusiness, String methodName, String operatorUser) {
	logger.info("操作人:" + operatorUser +"所在类及方法"+methodName+ "操作业务描述" + operatingBusiness);
    }

    /**
     * 错误日志
     * 
     * @param operatingBusiness
     *            操作业务
     * @param methodName
     *            方法名
     * @param parameter
     *            对象类参数
     * @param request
     *            request类参数
     * @param exception
     *            报错异常
     */
    public static void addErrorLog(String operatingBusiness, String methodName, Object parameter,
	    HttpServletRequest request,Object exception) {
	HashMap<String, Object> resMap = getParameter(parameter, request);

	logger.error("操作业务描述：" + operatingBusiness + ",类+方法名：" + methodName
		+ ",传入参数---：" + resMap + ",错误信息：",(Throwable) exception);
    }
    
    

}

 