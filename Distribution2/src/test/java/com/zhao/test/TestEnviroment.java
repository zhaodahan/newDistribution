package com.zhao.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.zhao.dao.ItemInfoMapper;
import com.zhao.dao.OrderInfoMapper;
import com.zhao.po.OrderInfo;
import com.zhao.po.OrderInfoExample;
import com.zhao.po.OrderInfoExample.Criteria;

/**
 * 测试dao层的工作
 * @author lfy
 *推荐Spring的项目就可以使用Spring的单元测试，可以自动注入我们需要的组件
 *1、导入SpringTest模块
 *2、@ContextConfiguration指定Spring配置文件的位置
 *3、直接autowired要使用的组件即可
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
//配置事务的回滚,对数据库的增删改都会回滚,便于测试用例的循环利用
@Rollback(value = true)
@Transactional(transactionManager = "transactionManager")
public class TestEnviroment {

//	@Autowired 
//	private CarInfoMapper carInfoMapper;
//	
//	@Autowired
//	private CarService carService;
	
//	@Autowired
//	private DriverService driverService;
//	private  RoleInfoMapper roleInfoMapper;
//	private DriverCarInfoMapper driverCarInfoMapper;
//	private OrderInfoMapper orderInfoMapper;
	
	@Autowired
	private ItemInfoMapper itemInfoMapper;
	

	@Test
	public void testCRUD(){
     
		System.out.println("sdafsad");
	}
	
	

	
	
}
