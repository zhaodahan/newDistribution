package com.zhao.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.zhao.po.CarInfo;
import com.zhao.po.DriverCarInfo;



/**
 * 使用Spring测试模块提供的测试请求功能，测试curd请求的正确性 Spring4测试的时候，需要servlet3. 0的支持
 * 
 * @author lfy
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:applicationContext.xml",
		"file:WebContent/WEB-INF/dispatcherServlet-servlet.xml" })
// 配置事务的回滚,对数据库的增删改都会回滚,便于测试用例的循环利用
@Rollback(value = true)
@Transactional(transactionManager = "transactionManager")
public class DriverMvcTest {
	// 传入Springmvc的ioc
	@Autowired
	WebApplicationContext context;

	// 虚拟mvc请求，获取到处理结果。他被用来进行controller请求的单元测试
	MockMvc mockMvc;

	@Before
	public void initMokcMvc() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void testPage() throws Exception {
		// mockMvc.perform 模拟web请求拿到返回值
//		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/getdrivers").param("pn", "1")).andReturn();

		String responseString = mockMvc
				.perform(get("/getdrivers").contentType(MediaType.APPLICATION_JSON).param("pn", "1"))
				.andExpect(status().isOk()) // 返回的状态是200
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);

	}

	@Test
	public void testDelete() throws Exception {
		String responseString = mockMvc
				.perform(delete("/deletedriver/1")
						.contentType(MediaType.APPLICATION_JSON))
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);

	}

	@Test
	public void testGet() throws Exception {
		String responseString = mockMvc
				.perform(get("/getCar/1").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()) // 返回的状态是200
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);
	}

	/**
	 * 因为无法加载web.xml，所以更新无法测试
	 * @throws Exception
	 */
	@Test
	public void testUpdate() throws Exception {
		DriverCarInfo driverCarInfo=new DriverCarInfo();
		driverCarInfo.setDriverstate(false);
//		 将对象转换成json字符串
		String requestJson = JSONObject.toJSONString(driverCarInfo); 

		String responseString = mockMvc
				.perform(put("/updatedriver/1").contentType(MediaType.APPLICATION_JSON)
						.content(requestJson))
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);
	}
	
	
	/**
	 * 测试 添加
	 * @throws Exception
	 */
	@Test
	public void testAdd() throws Exception {
		
		DriverCarInfo driverCarInfo=new DriverCarInfo();
		driverCarInfo.setDriverstate(false);
		driverCarInfo.setDrivername("陈云");
		driverCarInfo.setTelphone("15310384457");
		driverCarInfo.setIdentityid("5002381995055057494");
//		 将对象转换成json字符串
		String requestJson = JSONObject.toJSONString(driverCarInfo); 

		System.out.println(requestJson);
		String responseString = mockMvc
				.perform(post("/adddriver").contentType(MediaType.APPLICATION_JSON)
						.content(requestJson))
				.andDo(print()) // 打印出请求和相应的内容
				.andReturn().getResponse().getContentAsString(); // 将相应的数据转换为字符串
		System.out.println("--------返回的json = " + responseString);
	}
	
	
}
