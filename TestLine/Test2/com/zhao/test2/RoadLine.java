package com.zhao.test2;

public class RoadLine {

	private int end; //路线的终点

	private int id; //路线的id

	private int length; //路线的长度

	private int  start; //路线的起始点

	public int getEnd() {
		return end;
	}

	public int getId() {
		return id;
	}

	public int getLength() {
		return length;
	}

	public int getStart() {
		return start;
	}

	public void setEnd(int end) {
		this.end = end;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	public void setStart(int start) {
		this.start = start;
	}

	public RoadLine( int id, int start,int end, int length) {
		super();
		this.end = end;
		this.id = id;
		this.length = length;
		this.start = start;
	}
	
	
	
	
}
