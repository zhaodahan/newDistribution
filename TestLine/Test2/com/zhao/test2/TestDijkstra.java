package com.zhao.test2;

import java.util.ArrayList;
import java.util.List;

import com.zhao.FileUtil;
import com.zhao.test2.RoadLine;

public class TestDijkstra {
	public static void main(String[] args) {
//		test1(args);
		line2();
	}

	public static void test1(String[] args) {
		String graphFilePath;
		if (args.length == 0)
			graphFilePath = "G:\\road3.txt";
		else
			graphFilePath = args[0];

		String graphContent = FileUtil.read(graphFilePath, null);
		WeightedGraph graph = new WeightedGraph(graphContent);
		graph.dijkstra();

		graph.showDistance();
	}

	public static void line2() {
		List<RoadLine> roadLines = new ArrayList<RoadLine>();
		roadLines.add(new RoadLine(0, 0, 1, 4));
		roadLines.add(new RoadLine(1, 0, 2, 7));
		roadLines.add(new RoadLine(2, 0, 3, 3));
		roadLines.add(new RoadLine(3, 1, 2, 3));
		roadLines.add(new RoadLine(4, 1, 4, 2));
		roadLines.add(new RoadLine(5, 3, 4, 3));
		roadLines.add(new RoadLine(6, 2, 5, 2));
		roadLines.add(new RoadLine(7, 4, 5, 2));

		MyWeightedGraph graph = new MyWeightedGraph(roadLines);

		System.out.println(graph.getMinStep(0, 5));

	}
}