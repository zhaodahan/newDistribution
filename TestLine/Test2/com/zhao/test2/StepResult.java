package com.zhao.test2;

public class StepResult {

	private int length;

	private String minstep;

	public int getLength() {
		return length;
	}

	public String getMinstep() {
		return minstep;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	public void setMinstep(String minstep) {
		this.minstep = minstep;
	}
	
	
}
