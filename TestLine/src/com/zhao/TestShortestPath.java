package com.zhao;

import java.util.ArrayList;
import java.util.List;

public class TestShortestPath {
    public static void main(String[] args) {
        test1(args);
//    	line2();
    }


	public static void test1(String[] args) {
		String graphFilePath;
        if(args.length == 0)
            graphFilePath = "G:\\road.txt";
        else
            graphFilePath = args[0];
        
        String graphContent = FileUtil.read(graphFilePath, null);        
        MyNonDirectedGraph graph = new MyNonDirectedGraph(graphContent);
      graph.unweightedShortestPath();
      graph.showDistance();
	}
    
    
    public static void line2() {
          List<RoadLine> roadLines=new ArrayList<RoadLine>();
    	  roadLines.add(new RoadLine(0, "0", "1", 4));
    	  roadLines.add(new RoadLine(1, "0", "2", 7));
    	  roadLines.add(new RoadLine(2, "0", "3", 3));
    	  roadLines.add(new RoadLine(3, "1", "2", 3));
    	  roadLines.add(new RoadLine(4, "1", "4", 2));
    	  roadLines.add(new RoadLine(5, "3", "4", 3));
    	  roadLines.add(new RoadLine(6, "2", "5", 2));
    	  roadLines.add(new RoadLine(7, "4", "5", 2));
          
          
           NonDirectedGraph graph = new NonDirectedGraph(roadLines);
           graph.unweightedShortestPath();
           graph.showDistance();
          
	}
}