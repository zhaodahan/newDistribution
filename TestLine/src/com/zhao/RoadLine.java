package com.zhao;

public class RoadLine {

	private String end; //路线的终点

	private int id; //路线的id

	private int length; //路线的长度

	private String  start; //路线的起始点

	public String getEnd() {
		return end;
	}

	public int getId() {
		return id;
	}

	public int getLength() {
		return length;
	}

	public String getStart() {
		return start;
	}

	public void setEnd(String end) {
		this.end = end;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setLength(int length) {
		this.length = length;
	}
	
	public void setStart(String start) {
		this.start = start;
	}

	public RoadLine( int id, String start,String end, int length) {
		super();
		this.end = end;
		this.id = id;
		this.length = length;
		this.start = start;
	}
	
	
	
	
}
